namespace Database
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DeduccionesPorPersona")]
    public partial class DeduccionesPorPersona
    {
        [Key]
        public int IdDeduccionesPorPersona { get; set; }

        public int IdPersonas { get; set; }

        public int IdDeduccion { get; set; }

        [StringLength(50)]
        public string EstadoDeduccionesPorPersona { get; set; }
    }
}

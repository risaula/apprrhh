namespace Database
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Ingresos")]
    public partial class Ingreso
    {
        [Key]
        public int IdIngresos { get; set; }

        [StringLength(50)]
        public string Descripcion { get; set; }

        [StringLength(50)]
        public string Leyenda { get; set; }

        [StringLength(50)]
        public string EstadoIngresos { get; set; }
    }
}

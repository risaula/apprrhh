namespace Database
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class HistoricoEmpresaPorPersona
    {
        [Key]
        public int IdHistoricoEmpresaPorPersonas { get; set; }

        public int? IdCompañia { get; set; }

        public int? IdPersonas { get; set; }

        [StringLength(50)]
        public string EstadoHistoricoEmpresaPorPersonas { get; set; }
    }
}

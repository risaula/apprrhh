namespace Database
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DocumentosPorPersona")]
    public partial class DocumentosPorPersona
    {
        [Key]
        public int IdDocumentosPorPersona { get; set; }

        public int? IdTipoDocumento { get; set; }

        [StringLength(50)]
        public string Documento { get; set; }

        [StringLength(50)]
        public string EstadoDocumentosPorPersona { get; set; }

        public int IdPersonas { get; set; }
    }
}

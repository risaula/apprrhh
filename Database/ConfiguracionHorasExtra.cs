namespace Database
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ConfiguracionHorasExtra
    {
        [Key]
        public int IdConfiguracionHorasExtras { get; set; }

        [StringLength(50)]
        public string Descripcion { get; set; }

        public decimal? PorcentajeAplicar { get; set; }

        public int IdTurno { get; set; }

        public int? MaxHoras { get; set; }

        [StringLength(50)]
        public string EstadoConfiguracionHorasExtras { get; set; }
    }
}

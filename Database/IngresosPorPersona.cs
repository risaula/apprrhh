namespace Database
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("IngresosPorPersona")]
    public partial class IngresosPorPersona
    {
        [Key]
        public int IdIngresosPorPersona { get; set; }

        public int IdPersonas { get; set; }

        public int IdIngresos { get; set; }

        [StringLength(50)]
        public string EstadoIngresosPorPersona { get; set; }
    }
}

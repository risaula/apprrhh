namespace Database
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Persona
    {
        [Key]
        public int IdPersonas { get; set; }

        public int? IdTipoPersona { get; set; }

        [StringLength(50)]
        public string Nombre { get; set; }

        [StringLength(50)]
        public string Apellido { get; set; }

        public int? NumeroIdentidad { get; set; }

        [StringLength(10)]
        public string Direccion { get; set; }

        [StringLength(50)]
        public string TelefonoMovil { get; set; }

        [StringLength(50)]
        public string TelefonoFijo { get; set; }

        public DateTime? FechaNacimiento { get; set; }

        [StringLength(50)]
        public string PoseeVehiculo { get; set; }

        public int IdNacionalidad { get; set; }

        public int IdEstadoCivil { get; set; }

        [StringLength(50)]
        public string EstadoContrato { get; set; }

        [StringLength(50)]
        public string EstadoPersona { get; set; }

        public int IdPuesto { get; set; }

        public int IdTurno { get; set; }

        public int? CodigoEmpleado { get; set; }

        public int? IdTipoGenero { get; set; }
    }
}

namespace Database
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ParientesPorPersona")]
    public partial class ParientesPorPersona
    {
        [Key]
        public int IdParientesPorPersonas { get; set; }

        public int IdPersonas { get; set; }

        [StringLength(50)]
        public string Nombre { get; set; }

        [StringLength(50)]
        public string Apellido { get; set; }

        [StringLength(50)]
        public string Direccion { get; set; }

        [StringLength(50)]
        public string Telefono { get; set; }

        public int TipoParentesco { get; set; }

        [StringLength(50)]
        public string EstadoParientesPorPersonas { get; set; }
    }
}

namespace Database
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model1 : DbContext
    {
        public Model1()
            : base("name=RRHH")
        {
        }

        public virtual DbSet<Area> Areas { get; set; }
        public virtual DbSet<CentroEducativo> CentroEducativoes { get; set; }
        public virtual DbSet<Ciudad> Ciudads { get; set; }
        public virtual DbSet<ConfiguracionHorasExtra> ConfiguracionHorasExtras { get; set; }
        public virtual DbSet<ConfiguracionHorasExtrasPorPersona> ConfiguracionHorasExtrasPorPersonas { get; set; }
        public virtual DbSet<Deduccione> Deducciones { get; set; }
        public virtual DbSet<DeduccionesPorPersona> DeduccionesPorPersonas { get; set; }
        public virtual DbSet<DocumentosPorPersona> DocumentosPorPersonas { get; set; }
        public virtual DbSet<EstadoCivil> EstadoCivils { get; set; }
        public virtual DbSet<HistorialPuestoPorPersona> HistorialPuestoPorPersonas { get; set; }
        public virtual DbSet<HistoricoContratoPorPersona> HistoricoContratoPorPersonas { get; set; }
        public virtual DbSet<HistoricoEmpresaPorPersona> HistoricoEmpresaPorPersonas { get; set; }
        public virtual DbSet<HistoricoSueldoPorPesona> HistoricoSueldoPorPesonas { get; set; }
        public virtual DbSet<HistoricoTurnoPorPersona> HistoricoTurnoPorPersonas { get; set; }
        public virtual DbSet<Idioma> Idiomas { get; set; }
        public virtual DbSet<IdiomasPorPersona> IdiomasPorPersonas { get; set; }
        public virtual DbSet<Ingreso> Ingresos { get; set; }
        public virtual DbSet<IngresosPorPersona> IngresosPorPersonas { get; set; }
        public virtual DbSet<Nacionalidad> Nacionalidads { get; set; }
        public virtual DbSet<NivelEducativo> NivelEducativoes { get; set; }
        public virtual DbSet<NivelEducativoPorPersona> NivelEducativoPorPersonas { get; set; }
        public virtual DbSet<Pai> Pais { get; set; }
        public virtual DbSet<ParientesPorPersona> ParientesPorPersonas { get; set; }
        public virtual DbSet<Persona> Personas { get; set; }
        public virtual DbSet<Profesione> Profesiones { get; set; }
        public virtual DbSet<Puesto> Puestoes { get; set; }
        public virtual DbSet<TipoContrato> TipoContratoes { get; set; }
        public virtual DbSet<TipoDocumento> TipoDocumentoes { get; set; }
        public virtual DbSet<TipoEmpresa> TipoEmpresas { get; set; }
        public virtual DbSet<TipoGenero> TipoGeneroes { get; set; }
        public virtual DbSet<TipoMotivoRetiro> TipoMotivoRetiroes { get; set; }
        public virtual DbSet<TipoParentesco> TipoParentescoes { get; set; }
        public virtual DbSet<TipoPersona> TipoPersonas { get; set; }
        public virtual DbSet<TipoPlanilla> TipoPlanillas { get; set; }
        public virtual DbSet<Turno> Turnoes { get; set; }
        public virtual DbSet<Departamento> Departamentoes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ConfiguracionHorasExtra>()
                .Property(e => e.PorcentajeAplicar)
                .HasPrecision(18, 0);

            modelBuilder.Entity<ConfiguracionHorasExtrasPorPersona>()
                .Property(e => e.EstadoConfiguracionHorasExtrasPorPersona)
                .IsFixedLength();

            modelBuilder.Entity<HistoricoSueldoPorPesona>()
                .Property(e => e.MontoPorHora)
                .HasPrecision(18, 0);

            modelBuilder.Entity<HistoricoSueldoPorPesona>()
                .Property(e => e.Monto)
                .HasPrecision(18, 0);

            modelBuilder.Entity<Persona>()
                .Property(e => e.Direccion)
                .IsFixedLength();

            modelBuilder.Entity<Profesione>()
                .Property(e => e.EstadoProfesiones)
                .IsFixedLength();

            modelBuilder.Entity<Puesto>()
                .Property(e => e.Estado)
                .IsFixedLength();

            modelBuilder.Entity<TipoContrato>()
                .Property(e => e.EstadoTipoContrato)
                .IsFixedLength();

            modelBuilder.Entity<TipoParentesco>()
                .Property(e => e.EstadoParentesco)
                .IsFixedLength();
        }
    }
}

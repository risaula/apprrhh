namespace Database
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("IdiomasPorPersona")]
    public partial class IdiomasPorPersona
    {
        [Key]
        public int IdIdiomasPorPersona { get; set; }

        public int? IdIdiomas { get; set; }

        public int? IdPersonas { get; set; }
    }
}

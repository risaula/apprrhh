namespace Database
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("NivelEducativo")]
    public partial class NivelEducativo
    {
        [Key]
        public int IdNivelEducativo { get; set; }

        [StringLength(50)]
        public string Descripcion { get; set; }

        [StringLength(50)]
        public string EstadoNivelEducativo { get; set; }
    }
}

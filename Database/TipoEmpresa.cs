namespace Database
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TipoEmpresa")]
    public partial class TipoEmpresa
    {
        [Key]
        public int IdTipoEmpresa { get; set; }

        [StringLength(50)]
        public string Descripcion { get; set; }

        [StringLength(50)]
        public string EstadoTipoEmpresa { get; set; }
    }
}

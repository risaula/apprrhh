namespace Database
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Deduccione
    {
        [Key]
        public int IdDeducciones { get; set; }

        [StringLength(50)]
        public string Descripcion { get; set; }

        [StringLength(50)]
        public string Leyevnda { get; set; }

        [StringLength(50)]
        public string Acumulativo { get; set; }

        [StringLength(50)]
        public string EstadoDeducciones { get; set; }
    }
}

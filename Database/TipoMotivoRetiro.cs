namespace Database
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TipoMotivoRetiro")]
    public partial class TipoMotivoRetiro
    {
        [Key]
        public int IdTipoMotivoRetiro { get; set; }

        [StringLength(50)]
        public string Descripcion { get; set; }

        [StringLength(50)]
        public string EstadoTipoMotivoRetiro { get; set; }
    }
}

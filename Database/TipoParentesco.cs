namespace Database
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TipoParentesco")]
    public partial class TipoParentesco
    {
        [Key]
        public int IdTipoParentesco { get; set; }

        [StringLength(50)]
        public string Descripcion { get; set; }

        [StringLength(10)]
        public string EstadoParentesco { get; set; }
    }
}

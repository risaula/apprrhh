namespace Database
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("HistoricoSueldoPorPesona")]
    public partial class HistoricoSueldoPorPesona
    {
        [Key]
        public int IdHistoricoSueldoPorPesona { get; set; }

        public int IdPersonas { get; set; }

        public int IdTipoPlanilla { get; set; }

        public decimal? MontoPorHora { get; set; }

        public decimal? Monto { get; set; }

        [StringLength(50)]
        public string EstadoHistoricoSueldoPorPesona { get; set; }
    }
}

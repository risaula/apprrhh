namespace Database
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Puesto")]
    public partial class Puesto
    {
        [Key]
        public int IdPuesto { get; set; }

        [StringLength(50)]
        public string DescripcionPuesto { get; set; }

        public int IdArea { get; set; }

        [StringLength(10)]
        public string Estado { get; set; }
    }
}

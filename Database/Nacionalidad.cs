namespace Database
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Nacionalidad")]
    public partial class Nacionalidad
    {
        [Key]
        public int IdNacionalidad { get; set; }

        [StringLength(50)]
        public string Descripcion { get; set; }

        [StringLength(50)]
        public string FormatoIdentidad { get; set; }

        [StringLength(50)]
        public string FormatoTelefonico { get; set; }

        [StringLength(50)]
        public string EstadoNacionalidad { get; set; }
    }
}

namespace Database
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("NivelEducativoPorPersona")]
    public partial class NivelEducativoPorPersona
    {
        [Key]
        public int IdNivelEducativoPorPersona { get; set; }

        public int IdPersonas { get; set; }

        public int IdNivelEducativo { get; set; }

        public int IdProfesiones { get; set; }

        [Required]
        [StringLength(50)]
        public string EstadoNivelEducativo { get; set; }

        public int IdCentroEducativo { get; set; }
    }
}

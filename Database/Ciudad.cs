namespace Database
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Ciudad")]
    public partial class Ciudad
    {
        [Key]
        public int IdCiudad { get; set; }

        [Column("Ciudad")]
        [StringLength(50)]
        public string Ciudad1 { get; set; }

        public int IdPais { get; set; }

        [StringLength(50)]
        public string EstadoCiudad { get; set; }
    }
}

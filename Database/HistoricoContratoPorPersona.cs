namespace Database
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("HistoricoContratoPorPersona")]
    public partial class HistoricoContratoPorPersona
    {
        [Key]
        public int IdHistoricoContratoPorEmpleado { get; set; }

        public int IdTipoContrato { get; set; }

        public int IdPersonas { get; set; }

        [StringLength(50)]
        public string EstadoHistoricoContratoPorPersona { get; set; }
    }
}

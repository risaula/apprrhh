namespace Database
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ConfiguracionHorasExtrasPorPersona")]
    public partial class ConfiguracionHorasExtrasPorPersona
    {
        [Key]
        public int IdConfiguracionHorasExtrasPorPersona { get; set; }

        public int IdConfiguracionHorasExtras { get; set; }

        public int IdPersonas { get; set; }

        [Column(TypeName = "date")]
        public DateTime? FechaAplicacion { get; set; }

        [StringLength(10)]
        public string EstadoConfiguracionHorasExtrasPorPersona { get; set; }
    }
}

namespace Database
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("HistorialPuestoPorPersona")]
    public partial class HistorialPuestoPorPersona
    {
        [Key]
        public int IdHistorialPuestoPorPersona { get; set; }

        public int IdPersonas { get; set; }

        public int IdPuesto { get; set; }

        [StringLength(50)]
        public string EstadoHistorialPuestoPorPersona { get; set; }
    }
}

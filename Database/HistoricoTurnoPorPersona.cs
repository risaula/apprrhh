namespace Database
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("HistoricoTurnoPorPersona")]
    public partial class HistoricoTurnoPorPersona
    {
        [Key]
        public int IdHistoricoTurnoPorPersona { get; set; }

        public int IdTurno { get; set; }

        public int IdPersona { get; set; }

        [StringLength(50)]
        public string EstadoHistoricoTurnoPorPersona { get; set; }
    }
}

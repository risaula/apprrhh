
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace SmartAdminMvc.Database
{

using System;
    using System.Collections.Generic;
    
public partial class TipoMotivoRetiro
{

    public int IdTipoMotivoRetiro { get; set; }

    public string Descripcion { get; set; }

    public bool EstadoTipoMotivoRetiro { get; set; }

    public System.DateTime FechaCreacion { get; set; }

    public System.DateTime FechaModificado { get; set; }

    public string CreadoPor { get; set; }

    public string ModificadoPor { get; set; }

}

}

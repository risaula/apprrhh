
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace SmartAdminMvc.Database
{

using System;
    using System.Collections.Generic;
    
public partial class Persona
{

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
    public Persona()
    {

        this.ConfiguracionHorasExtrasPorPersonas = new HashSet<ConfiguracionHorasExtrasPorPersona>();

        this.DeduccionesPorPersonas = new HashSet<DeduccionesPorPersona>();

        this.DocumentosPorPersonas = new HashSet<DocumentosPorPersona>();

        this.HistorialPuestoPorPersonas = new HashSet<HistorialPuestoPorPersona>();

        this.HistoricoContratoPorPersonas = new HashSet<HistoricoContratoPorPersona>();

        this.HistoricoEmpresaPorPersonas = new HashSet<HistoricoEmpresaPorPersona>();

        this.HistoricoSueldoPorPesonas = new HashSet<HistoricoSueldoPorPesona>();

        this.HistoricoTurnoPorPersonas = new HashSet<HistoricoTurnoPorPersona>();

        this.IdiomasPorPersonas = new HashSet<IdiomasPorPersona>();

        this.IngresosPorPersonas = new HashSet<IngresosPorPersona>();

        this.NivelEducativoPorPersonas = new HashSet<NivelEducativoPorPersona>();

        this.Observaciones = new HashSet<Observacione>();

        this.ParientesPorPersonas = new HashSet<ParientesPorPersona>();

    }


    public int IdPersonas { get; set; }

    public Nullable<int> IdTipoPersona { get; set; }

    public string Nombre { get; set; }

    public string Apellido { get; set; }

    public string NumeroIdentidad { get; set; }

    public string Direccion { get; set; }

    public string TelefonoMovil { get; set; }

    public string TelefonoFijo { get; set; }

    public System.DateTime FechaNacimiento { get; set; }

    public bool PoseeVehiculo { get; set; }

    public int IdNacionalidad { get; set; }

    public int IdEstadoCivil { get; set; }

    public string EstadoContrato { get; set; }

    public bool EstadoPersona { get; set; }

    public Nullable<int> IdPuesto { get; set; }

    public Nullable<int> IdTurno { get; set; }

    public Nullable<int> CodigoEmpleado { get; set; }

    public int IdTipoGenero { get; set; }

    public System.DateTime FechaCreacion { get; set; }

    public System.DateTime FechaModificado { get; set; }

    public string CreadoPor { get; set; }

    public string ModificadoPor { get; set; }

    public Nullable<System.DateTime> FechaIncio { get; set; }

    public Nullable<bool> EstadoCandidato { get; set; }

    public Nullable<int> Edad { get; set; }



    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]

    public virtual ICollection<ConfiguracionHorasExtrasPorPersona> ConfiguracionHorasExtrasPorPersonas { get; set; }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]

    public virtual ICollection<DeduccionesPorPersona> DeduccionesPorPersonas { get; set; }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]

    public virtual ICollection<DocumentosPorPersona> DocumentosPorPersonas { get; set; }

    public virtual EstadoCivil EstadoCivil { get; set; }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]

    public virtual ICollection<HistorialPuestoPorPersona> HistorialPuestoPorPersonas { get; set; }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]

    public virtual ICollection<HistoricoContratoPorPersona> HistoricoContratoPorPersonas { get; set; }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]

    public virtual ICollection<HistoricoEmpresaPorPersona> HistoricoEmpresaPorPersonas { get; set; }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]

    public virtual ICollection<HistoricoSueldoPorPesona> HistoricoSueldoPorPesonas { get; set; }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]

    public virtual ICollection<HistoricoTurnoPorPersona> HistoricoTurnoPorPersonas { get; set; }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]

    public virtual ICollection<IdiomasPorPersona> IdiomasPorPersonas { get; set; }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]

    public virtual ICollection<IngresosPorPersona> IngresosPorPersonas { get; set; }

    public virtual Nacionalidad Nacionalidad { get; set; }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]

    public virtual ICollection<NivelEducativoPorPersona> NivelEducativoPorPersonas { get; set; }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]

    public virtual ICollection<Observacione> Observaciones { get; set; }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]

    public virtual ICollection<ParientesPorPersona> ParientesPorPersonas { get; set; }

    public virtual TipoGenero TipoGenero { get; set; }

    public virtual TipoPersona TipoPersona { get; set; }

}

}

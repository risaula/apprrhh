
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace SmartAdminMvc.Database
{

using System;
    using System.Collections.Generic;
    
public partial class Pai
{

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
    public Pai()
    {

        this.Ciudads = new HashSet<Ciudad>();

    }


    public int IdPais { get; set; }

    public string Pais { get; set; }

    public bool EstadoPais { get; set; }

    public System.DateTime FechaCreacion { get; set; }

    public System.DateTime FechaModificado { get; set; }

    public string CreadoPor { get; set; }

    public string ModificadoPor { get; set; }



    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]

    public virtual ICollection<Ciudad> Ciudads { get; set; }

}

}

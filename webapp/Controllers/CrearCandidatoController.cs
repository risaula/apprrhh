﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.Mvc;
using SmartAdminMvc.Database;
using SmartAdminMvc.Models.Idiomas;
using SmartAdminMvc.Models.Personas;
using SmartAdminMvc.Controllers;
using SmartAdminMvc.Models.Areas;
using SmartAdminMvc.Models.Base;
using SmartAdminMvc.Models.Genero;

namespace SmartAdminMvc.Controllers
{
    [Authorize]
    public class CrearCandidatoController : Controller
    {
        // GET: CrearCandidato
        public ActionResult Index()
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {
                var ListarPersona = dbRecursosHumanosEntities.Personas.Where(x => x.EstadoCandidato == false).Select(x => new CrearCandidatoViewModel
                {

                    IdPersona = x.IdPersonas,
                    Nombres = x.Nombre + " " + x.Apellido,
                    NumeroIdentidads = x.NumeroIdentidad,
                    Direccions = x.Direccion,
                    TelefonoFijos = x.TelefonoFijo,
                    TelefonoMovils = x.TelefonoMovil,
                    Persona = x.TipoPersona.IdTipoPersona.ToString(),
                    EstadoCivils = x.EstadoCivil.Descripcion,
                    Generos = x.TipoGenero.Descripcion,
                    Nacionalidads = x.Nacionalidad.Descripcion,
                    FechaNacimientos = x.FechaNacimiento,
                    PoseeVehiculos = x.PoseeVehiculo,
                    Estado = x.EstadoPersona,

                }).ToList();
                return View(ListarPersona);

            }
        }

        // GET: CrearCandidato/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: CrearCandidato/Create
        public ActionResult Create()
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {
                ViewBag.SelectGenero = dbRecursosHumanosEntities.TipoGeneroes.Where(x=>x.EstadoTipoGenero==true).ToList()
                    .Select(x => new SelectListItem
                    {
                        Text = x.Descripcion,
                        Value = x.IdTipoGenero.ToString()
                    }).ToList();
                ViewBag.SelectEstadoCivil = dbRecursosHumanosEntities.EstadoCivils.Where(x=>x.EstadoEstadoCivil==true).ToList()
                    .Select(x => new SelectListItem
                    {
                        Text = x.Descripcion,
                        Value = x.IdEstadoCivil.ToString()
                    }).ToList();
                ViewBag.SelectNacionalidad = dbRecursosHumanosEntities.Nacionalidads.Where(x=>x.EstadoNacionalidad==true).ToList()
                    .Select(x => new SelectListItem
                    {
                        Text = x.Descripcion,
                        Value = x.IdNacionalidad.ToString()
                    }).ToList();

                return View(new CrearCandidatoViewModel { FechaNacimientos = DateTime.Now });

            }
        }

        // POST: CrearCandidato/Create
        [HttpPost]
        public JsonResult Create(CrearCandidatoViewModel objPersona)
        {

            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Estatus = false, Mensaje = "Revisar campos requeridos." },
                        JsonRequestBehavior.AllowGet);
                }


                using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
                {

                    var persona = new Persona
                    {
                        IdPersonas = objPersona.IdPersona,
                        EstadoCandidato = objPersona.TipoPerso=false,
                        //IdTipoPersona = objPersona.IdTipoP = 1,
                       Apellido = objPersona.Apellido,
                        Nombre = objPersona.Nombres,
                        Direccion = objPersona.Direccions,
                        TelefonoFijo = objPersona.TelefonoFijos,
                        TelefonoMovil = objPersona.TelefonoMovils,
                        IdEstadoCivil = Convert.ToInt32(objPersona.EstadoCivils),
                        IdTipoGenero = Convert.ToInt32(objPersona.Generos),
                        IdNacionalidad = Convert.ToInt32(objPersona.Nacionalidads),
                        FechaNacimiento = objPersona.FechaNacimientos,
                        CreadoPor = User.Identity.Name,
                        ModificadoPor = User.Identity.Name,
                        FechaCreacion = DateTime.Now,
                        FechaModificado = DateTime.Now,
                      NumeroIdentidad = objPersona.NumeroIdentidads,
                        PoseeVehiculo = objPersona.PoseeVehiculos,
                        EstadoPersona =true,
             


                    };
                    dbRecursosHumanosEntities.Personas.Add(persona);
                    dbRecursosHumanosEntities.SaveChanges();
                    TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha guardado de manera exitosa.", Titulo = "Persona" };
                    return Json(new { Estatus = true, Mensaje = "Se ha  guardado con exito." },
                        JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                return Json(new { Estatus = false, Mensaje = ex.Message },
                    JsonRequestBehavior.AllowGet);
            }
        }

        // GET: CrearCandidato/Edit/5
        public ActionResult Edit(int id)
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {
                ViewBag.SelectGenero = dbRecursosHumanosEntities.TipoGeneroes.Where(x=>x.EstadoTipoGenero==true).ToList()
                    .Select(x => new SelectListItem
                    {
                        Text = x.Descripcion,
                        Value = x.IdTipoGenero.ToString()
                    }).ToList();
                ViewBag.SelectEstadoCivil = dbRecursosHumanosEntities.EstadoCivils.Where(x=>x.EstadoEstadoCivil==true).ToList()
                    .Select(x => new SelectListItem
                    {
                        Text = x.Descripcion,
                        Value = x.IdEstadoCivil.ToString()
                    }).ToList();
                ViewBag.SelectNacionalidad = dbRecursosHumanosEntities.Nacionalidads.Where(x=>x.EstadoNacionalidad==true).ToList()
                    .Select(x => new SelectListItem
                    {
                        Text = x.Descripcion,
                        Value = x.IdNacionalidad.ToString()
                    }).ToList();
                var query = dbRecursosHumanosEntities.Personas.FirstOrDefault(x => x.IdPersonas == id);

                var modelo = new CrearCandidatoViewModel
                {
                    IdPersona = query.IdPersonas,
                    Apellido = query.Apellido,
                    Nombres = query.Nombre,
                    Direccions = query.Direccion,
                    TelefonoMovils = query.TelefonoMovil,
                    TelefonoFijos = query.TelefonoFijo,
                    EstadoCivils = query.IdEstadoCivil.ToString(),
                    Generos = query.IdTipoGenero.ToString(),
                    Nacionalidads = query.IdNacionalidad.ToString(),
                   FechaNacimientos = query.FechaNacimiento,
                   NumeroIdentidads = query.NumeroIdentidad,
                    PoseeVehiculos = query.PoseeVehiculo,
                    Estado = true,

                };

                return View(modelo);

            }
        }

        // POST: CrearCandidato/Edit/5
        [HttpPost]
        public ActionResult Edit(CrearCandidatoViewModel objPersona)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Estatus = false, Mensaje = "Revisar campos requeridos." },
                        JsonRequestBehavior.AllowGet);
                }
                using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
                {

                    var persona =
                        dbRecursosHumanosEntities.Personas.FirstOrDefault(x => x.IdPersonas == objPersona.IdPersona);
                    persona.Apellido = objPersona.Apellido;
                    persona.Nombre = objPersona.Nombres;
                    persona.Direccion = objPersona.Direccions;
                    persona.TelefonoFijo = objPersona.TelefonoFijos;
                    persona.TelefonoMovil = objPersona.TelefonoMovils;
                    persona.IdEstadoCivil = Convert.ToInt32(objPersona.EstadoCivils);
                    persona.IdTipoGenero = Convert.ToInt32(objPersona.Generos);
                    persona.IdNacionalidad = Convert.ToInt32(objPersona.Nacionalidads);
                    persona.FechaCreacion = DateTime.Now;
                    persona.FechaNacimiento=objPersona.FechaNacimientos;
                  persona.NumeroIdentidad = objPersona.NumeroIdentidads;
                    persona.EstadoContrato = objPersona.Contrato;
                    persona.PoseeVehiculo = objPersona.PoseeVehiculos;
                    persona.EstadoCandidato = objPersona.TipoPerso = false;
                    //persona.IdTipoPersona = objPersona.IdTipoP = 1;
                    persona.EstadoPersona = true;
                    dbRecursosHumanosEntities.Entry(persona).State = EntityState.Modified;
                    dbRecursosHumanosEntities.SaveChanges();

                    TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha guardado de manera exitosa.", Titulo = "Persona" };
                    return Json(new { Estatus = true, Mensaje = "Se ha  Editado con exito." },
                        JsonRequestBehavior.AllowGet);
                    //return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                return Json(new { Estatus = false, Mensaje = ex.Message },
                    JsonRequestBehavior.AllowGet);
            }
        }
        //Get
        public ActionResult Contratados(int id)
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {
                ViewBag.SelectGenero = dbRecursosHumanosEntities.TipoGeneroes.Where(x=>x.EstadoTipoGenero==true).ToList()
                    .Select(x => new SelectListItem
                    {
                        Text = x.Descripcion,
                        Value = x.IdTipoGenero.ToString()
                    }).ToList();
                ViewBag.SelectEstadoCivil = dbRecursosHumanosEntities.EstadoCivils.Where(x=>x.EstadoEstadoCivil==true).ToList()
                    .Select(x => new SelectListItem
                    {
                        Text = x.Descripcion,
                        Value = x.IdEstadoCivil.ToString()
                    }).ToList();
                ViewBag.SelectNacionalidad = dbRecursosHumanosEntities.Nacionalidads.Where(x=>x.EstadoNacionalidad==true).ToList()
                    .Select(x => new SelectListItem
                    {
                        Text = x.Descripcion,
                        Value = x.IdNacionalidad.ToString()
                    }).ToList();

                ViewBag.SelectPlanilla = dbRecursosHumanosEntities.TipoPlanillas.Where(x=>x.EstadoTipoPlanilla==true).ToList()
                    .Select(x => new SelectListItem
                    {
                        Text = x.Descripcion,
                        Value = x.IdTipoPlanilla.ToString()
                    }).ToList();
                ViewBag.SelectPuesto = dbRecursosHumanosEntities.Puestoes.Where(x=>x.Estado==true).ToList()
                    .Select(x => new SelectListItem
                    {
                        Text = x.DescripcionPuesto,
                        Value = x.IdPuesto.ToString()
                    }).ToList();
                ViewBag.SelectContrato = dbRecursosHumanosEntities.TipoContratoes.Where(x=>x.EstadoTipoContrato==true).ToList()
                    .Select(x => new SelectListItem
                    {
                        Text = x.Descripcion,
                        Value = x.IdTipoContrato.ToString()
                    }).ToList();
                ViewBag.SelectTurno = dbRecursosHumanosEntities.Turnoes.Where(x=>x.EstadoTurno==true).ToList()
                    .Select(x => new SelectListItem
                    {
                        Text = x.Descripcion,
                        Value = x.IdTurno.ToString()
                    }).ToList();
                var query = dbRecursosHumanosEntities.Personas.FirstOrDefault(x => x.IdPersonas == id);


                var modelo = new CrearCandidatoViewModel
                {

                    IdPersona = query.IdPersonas,
                    Apellido = query.Apellido,
                    Nombres = query.Nombre,
                    Direccions = query.Direccion,
                    TelefonoMovils = query.TelefonoMovil,
                    TelefonoFijos = query.TelefonoFijo,
                    EstadoCivils = query.IdEstadoCivil.ToString(),
                    Generos = query.IdTipoGenero.ToString(),
                    Nacionalidads = query.IdNacionalidad.ToString(),
                    FechaNacimientos = query.FechaNacimiento,
                    NumeroIdentidads = query.NumeroIdentidad,
                    PoseeVehiculos = query.PoseeVehiculo,
                    Estado = true,
                    FechaInic= DateTime.Now,
                    SueldoBase = query.HistoricoSueldoPorPesonas.Any(x => x.EstadoHistoricoSueldoPorPesona == true) ? Convert.ToDecimal(query.HistoricoSueldoPorPesonas.FirstOrDefault(x => x.EstadoHistoricoSueldoPorPesona == true).Monto) : 0.0m


                };

                return View(modelo);

            }
        }
        // POST: CrearCandidato/Edit/5
        [HttpPost]
        public ActionResult Contratados(CrearCandidatoViewModel objPersona)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Estatus = false, Mensaje = "Revisar campos requeridos." },
                        JsonRequestBehavior.AllowGet);
                }
                using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
                {

                    var persona =
                        dbRecursosHumanosEntities.Personas.FirstOrDefault(x => x.IdPersonas == objPersona.IdPersona);
                    persona.Apellido = objPersona.Apellido;
                    persona.Nombre = objPersona.Nombres;
                    persona.Direccion = objPersona.Direccions;
                    persona.TelefonoFijo = objPersona.TelefonoFijos;
                    persona.TelefonoMovil = objPersona.TelefonoMovils;
                    persona.IdEstadoCivil = Convert.ToInt32(objPersona.EstadoCivils);
                    persona.IdTipoGenero = Convert.ToInt32(objPersona.Generos);
                    persona.IdNacionalidad = Convert.ToInt32(objPersona.Nacionalidads);
                  persona.FechaNacimiento = objPersona.FechaNacimientos;
                 persona.NumeroIdentidad = objPersona.NumeroIdentidads;
                    persona.PoseeVehiculo = objPersona.PoseeVehiculos;
                    persona.EstadoCandidato = objPersona.TipoPerso = true;
                    //persona.IdTipoPersona = objPersona.IdTipoP = 2;
                    persona.EstadoPersona = true;
                    persona.CodigoEmpleado = objPersona.CodigoEmpleado;
                    persona.FechaIncio = objPersona.FechaInic;

                    var HistoricoTurno = new HistoricoTurnoPorPersona();
                    HistoricoTurno.IdPersona = objPersona.IdPersona;
                    HistoricoTurno.IdTurno = Convert.ToInt32(objPersona.TipoTurno);
                    HistoricoTurno.EstadoHistoricoTurnoPorPersona = objPersona.EstadoContrato;
                    dbRecursosHumanosEntities.HistoricoTurnoPorPersonas.Add(HistoricoTurno);

                    var HistoricoSueldo = new HistoricoSueldoPorPesona();
                    HistoricoSueldo.IdPersonas = objPersona.IdPersona;
                    HistoricoSueldo.IdTipoPlanilla = Convert.ToInt32(objPersona.TipoPlan);
                    HistoricoSueldo.Monto = objPersona.SueldoBase;
                    HistoricoSueldo.EstadoHistoricoSueldoPorPesona = objPersona.EstadoContrato;
                    dbRecursosHumanosEntities.HistoricoSueldoPorPesonas.Add(HistoricoSueldo);

                    var HistoricoPuesto = new HistorialPuestoPorPersona();
                    HistoricoPuesto.IdPersonas = objPersona.IdPersona;
                    HistoricoPuesto.IdPuesto = Convert.ToInt32(objPersona.Puestos);
                    HistoricoPuesto.EstadoHistorialPuestoPorPersona = objPersona.EstadoContrato;
                    dbRecursosHumanosEntities.HistorialPuestoPorPersonas.Add(HistoricoPuesto);

                    var HistoricoContrato = new HistoricoContratoPorPersona();
                    HistoricoContrato.IdPersonas = objPersona.IdPersona;
                    HistoricoContrato.IdTipoContrato = Convert.ToInt32(objPersona.Contrato);
                    HistoricoContrato.EstadoHistoricoContratoPorPersona = objPersona.EstadoContrato;
                    dbRecursosHumanosEntities.HistoricoContratoPorPersonas.Add(HistoricoContrato);

                    dbRecursosHumanosEntities.Entry(persona).State = EntityState.Modified;
                    dbRecursosHumanosEntities.SaveChanges();

                    TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha guardado de manera exitosa.", Titulo = "Persona" };
                    return Json(new { Estatus = true, Mensaje = "Se ha  guardado con exito." },
                        JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                return Json(new { Estatus = false, Mensaje = ex.Message },
                    JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ListarContratados()
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {

              //  var ListarPersona = dbRecursosHumanosEntities.Personas.Where(x => x.EstadoCandidato == false).Select(x => new CrearCandidatoViewModel


               var ListarPersona = dbRecursosHumanosEntities.Personas.Where(x => x.EstadoCandidato == true).ToList().Select(x => new CrearCandidatoViewModel
                {
                    IdPersona = x.IdPersonas,
                  //  Persona = x.TipoPersona.Descripcion.ToString(),
                  //  Puestos = x.IdPuesto.ToString(),
                  CodigoEmpleado = Convert.ToInt32(x.CodigoEmpleado),
                    NumeroIdentidads = x.NumeroIdentidad,
                    Nombres = x.Nombre + " " +  x.Apellido,
                 //   Apellido = x.Apellido,
                    Direccions = x.Direccion,
                    TelefonoFijos = x.TelefonoFijo,
                    TelefonoMovils = x.TelefonoMovil,
                    EstadoCivils = x.EstadoCivil.Descripcion,
                    Generos = x.TipoGenero.Descripcion,
                    Estado = x.EstadoPersona,
                    
                    TipoTurno = x.HistoricoTurnoPorPersonas.Any(y => y.EstadoHistoricoTurnoPorPersona == true) ? Convert.ToString(x.HistoricoTurnoPorPersonas.FirstOrDefault(y => y.EstadoHistoricoTurnoPorPersona == true).Turno.Descripcion) : "N/D",
                    TipoContra = x.HistoricoContratoPorPersonas.Any(y => y.EstadoHistoricoContratoPorPersona == true) ? Convert.ToString(x.HistoricoContratoPorPersonas.FirstOrDefault(y => y.EstadoHistoricoContratoPorPersona == true).TipoContrato.Descripcion) : "N/D",
                    //HistorialSueldo = x.HistoricoSueldoPorPesonas.Any(y => y.EstadoHistoricoSueldoPorPesona == true )? Convert.ToString(x.HistoricoSueldoPorPesonas.FirstOrDefault(y=>y.EstadoHistoricoSueldoPorPesona==true).Monto)     
                    SueldoBase = x.HistoricoSueldoPorPesonas.Any(y => y.EstadoHistoricoSueldoPorPesona == true) ? Convert.ToDecimal(x.HistoricoSueldoPorPesonas.FirstOrDefault(y => y.EstadoHistoricoSueldoPorPesona == true).Monto) : 0.0m,
                    //SueldoBase = x.HistoricoSueldoPorPesonas.Any(y => y.EstadoHistoricoSueldoPorPesona == true) ? Convert.ToDecimal(x.HistoricoSueldoPorPesonas.FirstOrDefault(y => y.EstadoHistoricoSueldoPorPesona == true).Monto) : 0.0m
                    TPuesto = x.HistorialPuestoPorPersonas.Any(y => y.EstadoHistorialPuestoPorPersona == true) ? Convert.ToString(x.HistorialPuestoPorPersonas.FirstOrDefault(y => y.EstadoHistorialPuestoPorPersona == true).Puesto.DescripcionPuesto) : "N/D",

                    //  SueldoBase = Convert.ToDecimal(x.HistoricoSueldoPorPesonas)
                }).ToList();
                return View(ListarPersona);
            }
        }
        
        // GET: CrearCandidato/EditContratado/
        public ActionResult EditContratados(int id)
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {
                ViewBag.SelectGenero = dbRecursosHumanosEntities.TipoGeneroes.Where(x=>x.EstadoTipoGenero==true).ToList()
                    .Select(x => new SelectListItem
                    {
                        Text = x.Descripcion,
                        Value = x.IdTipoGenero.ToString()
                    }).ToList();
                ViewBag.SelectEstadoCivil = dbRecursosHumanosEntities.EstadoCivils.Where(x=>x.EstadoEstadoCivil==true).ToList()
                    .Select(x => new SelectListItem
                    {
                        Text = x.Descripcion,
                        Value = x.IdEstadoCivil.ToString()
                    }).ToList();
                ViewBag.SelectNacionalidad = dbRecursosHumanosEntities.Nacionalidads.Where(x=>x.EstadoNacionalidad==true).ToList()
                    .Select(x => new SelectListItem
                    {
                        Text = x.Descripcion,
                        Value = x.IdNacionalidad.ToString()
                    }).ToList();

                ViewBag.SelectPlanilla = dbRecursosHumanosEntities.TipoPlanillas.Where(x=>x.EstadoTipoPlanilla==true).ToList()
                    .Select(x => new SelectListItem
                    {
                        Text = x.Descripcion,
                        Value = x.IdTipoPlanilla.ToString()
                    }).ToList();
                ViewBag.SelectPuesto = dbRecursosHumanosEntities.Puestoes.Where(x=>x.Estado==true).ToList()
                    .Select(x => new SelectListItem
                    {
                        Text = x.DescripcionPuesto,
                        Value = x.IdPuesto.ToString()
                    }).ToList();
                ViewBag.SelectContrato = dbRecursosHumanosEntities.TipoContratoes.Where(x=>x.EstadoTipoContrato==true).ToList()
                    .Select(x => new SelectListItem
                    {
                        Text = x.Descripcion,
                        Value = x.IdTipoContrato.ToString()
                    }).ToList();
                ViewBag.SelectTurno = dbRecursosHumanosEntities.Turnoes.Where(x=>x.EstadoTurno==true).ToList()
                    .Select(x => new SelectListItem
                    {
                        Text = x.Descripcion,
                        Value = x.IdTurno.ToString()
                    }).ToList();
                var query = dbRecursosHumanosEntities.Personas.FirstOrDefault(x => x.IdPersonas == id);


                var modelo = new CrearCandidatoViewModel
                {

                    IdPersona = query.IdPersonas,
                    Apellido = query.Apellido,
                    Nombres = query.Nombre,
                    Direccions = query.Direccion,
                    TelefonoMovils = query.TelefonoMovil,
                    TelefonoFijos = query.TelefonoFijo,
                    EstadoCivils = query.IdEstadoCivil.ToString(),
                    Generos = query.IdTipoGenero.ToString(),
                    Nacionalidads = query.IdNacionalidad.ToString(),
                   FechaNacimientos = query.FechaNacimiento,
                 NumeroIdentidads = query.NumeroIdentidad,
                    PoseeVehiculos = query.PoseeVehiculo,
                    Estado = true,
                    CodigoEmpleado = Convert.ToInt32(query.CodigoEmpleado),
                  FechaInic =  Convert.ToDateTime(query.FechaIncio),
                 EstadoContrato = query.PoseeVehiculo,
                    TipoTurno = query.HistoricoTurnoPorPersonas.Any(x=>x.EstadoHistoricoTurnoPorPersona==true) ? Convert.ToString(query.HistoricoTurnoPorPersonas.FirstOrDefault(x=>x.EstadoHistoricoTurnoPorPersona==true).Turno.IdTurno):"N/D",
                    TipoContra = query.HistoricoContratoPorPersonas.Any(x=>x.EstadoHistoricoContratoPorPersona==true)? Convert.ToString(query.HistoricoContratoPorPersonas.FirstOrDefault(x=>x.EstadoHistoricoContratoPorPersona==true).TipoContrato.IdTipoContrato):"N/D",
                    TipoPlan = query.HistoricoSueldoPorPesonas.Any(x=>x.EstadoHistoricoSueldoPorPesona==true)? Convert.ToString(query.HistoricoSueldoPorPesonas.FirstOrDefault(x=> x.EstadoHistoricoSueldoPorPesona==true).TipoPlanilla.IdTipoPlanilla):"N/D",
                    TPuesto = query.HistorialPuestoPorPersonas.Any(x=>x.EstadoHistorialPuestoPorPersona==true)? Convert.ToString(query.HistorialPuestoPorPersonas.FirstOrDefault(x=>x.EstadoHistorialPuestoPorPersona==true).Puesto.IdPuesto):"N/D",
                    SueldoBase = query.HistoricoSueldoPorPesonas.Any(x => x.EstadoHistoricoSueldoPorPesona == true) ? Convert.ToDecimal(query.HistoricoSueldoPorPesonas.FirstOrDefault(x => x.EstadoHistoricoSueldoPorPesona == true).Monto) : 0.0m
                    

                };

                return View(modelo);

            }
        }
        // POST: CrearCandidato/EditContratado/5
        [HttpPost]
        public ActionResult EditContratados(CrearCandidatoViewModel objPersona)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Estatus = false, Mensaje = "Revisar campos requeridos." },
                        JsonRequestBehavior.AllowGet);
                }
                using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
                {

                    var persona =
                        dbRecursosHumanosEntities.Personas.FirstOrDefault(x => x.IdPersonas == objPersona.IdPersona);
                    persona.Apellido = objPersona.Apellido;
                    persona.Nombre = objPersona.Nombres;
                    persona.Direccion = objPersona.Direccions;
                    persona.TelefonoFijo = objPersona.TelefonoFijos;
                    persona.TelefonoMovil = objPersona.TelefonoMovils;
                    persona.IdEstadoCivil = Convert.ToInt32(objPersona.EstadoCivils);
                    persona.IdTipoGenero = Convert.ToInt32(objPersona.Generos);
                    persona.IdNacionalidad = Convert.ToInt32(objPersona.Nacionalidads);
                   persona.FechaNacimiento = objPersona.FechaNacimientos;
                   persona.NumeroIdentidad = objPersona.NumeroIdentidads;
                    persona.PoseeVehiculo = objPersona.PoseeVehiculos;
                    persona.EstadoCandidato = objPersona.TipoPerso = true;
                    //persona.IdTipoPersona = objPersona.IdTipoP = 2;
                    persona.EstadoPersona = true;
                    persona.CodigoEmpleado = objPersona.CodigoEmpleado;
                    persona.FechaIncio = objPersona.FechaInic;

                    var HistoricoTurno = new HistoricoTurnoPorPersona();
                    HistoricoTurno.IdPersona = objPersona.IdPersona;
                    HistoricoTurno.IdTurno = Convert.ToInt32(objPersona.TipoTurno);
                    HistoricoTurno.EstadoHistoricoTurnoPorPersona = objPersona.EstadoContrato;
                    dbRecursosHumanosEntities.HistoricoTurnoPorPersonas.Add(HistoricoTurno);

                    var HistoricoSueldo = new HistoricoSueldoPorPesona();
                    HistoricoSueldo.IdPersonas = objPersona.IdPersona;
                    HistoricoSueldo.IdTipoPlanilla = Convert.ToInt32(objPersona.TipoPlan);
                    HistoricoSueldo.Monto = objPersona.SueldoBase;
                    HistoricoSueldo.EstadoHistoricoSueldoPorPesona = objPersona.EstadoContrato;
                    dbRecursosHumanosEntities.HistoricoSueldoPorPesonas.Add(HistoricoSueldo);

                    var HistoricoPuesto = new HistorialPuestoPorPersona();
                    HistoricoPuesto.IdPersonas = objPersona.IdPersona;
                    HistoricoPuesto.IdPuesto = Convert.ToInt32(objPersona.TPuesto);
                    HistoricoPuesto.EstadoHistorialPuestoPorPersona = objPersona.EstadoContrato;
                    dbRecursosHumanosEntities.HistorialPuestoPorPersonas.Add(HistoricoPuesto);

                    var HistoricoContrato = new HistoricoContratoPorPersona();
                    HistoricoContrato.IdPersonas = objPersona.IdPersona;
                    HistoricoContrato.IdTipoContrato = Convert.ToInt32(objPersona.TipoContra);
                    HistoricoContrato.EstadoHistoricoContratoPorPersona = objPersona.EstadoContrato;
                    dbRecursosHumanosEntities.HistoricoContratoPorPersonas.Add(HistoricoContrato);

                    dbRecursosHumanosEntities.Entry(persona).State = EntityState.Modified;
                    dbRecursosHumanosEntities.SaveChanges();
                    TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha guardado de manera exitosa.", Titulo = "Persona" };
                    return Json(new { Estatus = true, Mensaje = "Se ha  Editado con exito." },
                        JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                return Json(new { Estatus = false, Mensaje = ex.Message },
                    JsonRequestBehavior.AllowGet);
            }
        }
        // GET: CrearCandidato/Delete/5
        public ActionResult ArchivarCandidato(int id)
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {

                var query = dbRecursosHumanosEntities.Personas.FirstOrDefault(x => x.IdPersonas == id);

                var modelo = new CrearCandidatoViewModel
                {


                    Estado = query.EstadoPersona = false,

                };
                dbRecursosHumanosEntities.SaveChanges();
                TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha modificado con exito.", Titulo = "Areas" };
                return Json(new { Estatu = true, Mensaje = "Se ha modificado con exito." },
                    JsonRequestBehavior.AllowGet);

            }
        }

        public ActionResult ArchivarPersona(int id)
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {

                var query = dbRecursosHumanosEntities.Personas.FirstOrDefault(x => x.IdPersonas== id);

                var modelo = new CrearCandidatoViewModel
                {


                    Estado = query.EstadoPersona = false,

                };
                dbRecursosHumanosEntities.SaveChanges();
                TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha modificado con exito.", Titulo = "Areas" };
                return Json(new { Estatu = true, Mensaje = "Se ha modificado con exito." },
                    JsonRequestBehavior.AllowGet);

            }
        }

        // POST: CrearCandidato/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

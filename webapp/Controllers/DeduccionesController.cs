﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SmartAdminMvc.Database;
using SmartAdminMvc.Models.Base;
using SmartAdminMvc.Models.Deducciones;
using SmartAdminMvc.Models.Idiomas;

namespace SmartAdminMvc.Controllers
{
    [Authorize]
    public class DeduccionesController : Controller
    {
        // GET: Deducciones
        public ActionResult Index()
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {

                var ListarDed = dbRecursosHumanosEntities.Deducciones.Select(
                    x => new ListarDeduccionesViewModel
                    {
                        IdDeducciones = x.IdDeducciones,
                        Descripcion = x.Descripcion,
                        EstadoDeducciones = x.EstadoDeducciones
                    }).ToList();
                return View(ListarDed);
            }
        }

        // GET: Deducciones/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Deducciones/Create
        public ActionResult Create()
        {
            return PartialView(new CrearDeduccionesViewModel());
        }

        // POST: Deducciones/Create
        [HttpPost]
        public JsonResult Create(CrearDeduccionesViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new {Estatus = false, Mensaje = "Revisar campos requeridos."},
                        JsonRequestBehavior.AllowGet);
                }

                using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
                {
                    var nuevoDed = new Deduccione
                    {
                        Descripcion = model.Descripcion,
                        EstadoDeducciones = true,
                        CreadoPor = User.Identity.Name,
                        ModificadoPor = User.Identity.Name,
                        FechaCreacion = DateTime.Now,
                        FechaModificado = DateTime.Now

                    };
                    dbRecursosHumanosEntities.Deducciones.Add(nuevoDed);
                    dbRecursosHumanosEntities.SaveChanges();

                    TempData["MensajeDeRespuesta"] = new RespuestaBase
                    {
                        Estatus = true,
                        Mensage = "Se ha guardado de manera exitosa.",
                        Titulo = "Deducciones"
                    };
                    return Json(new {Estatus = true, Mensaje = "Se ha  guardado con exito."},
                        JsonRequestBehavior.AllowGet);


                }
            }
            catch (Exception ex)
            {

                return Json(new {Estatus = false, Mensaje = ex.Message},
                    JsonRequestBehavior.AllowGet);

            }
        }

        // GET: Deducciones/Edit/5
            public ActionResult Edit(int id)
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {
                var query = dbRecursosHumanosEntities.Deducciones.FirstOrDefault(x => x.IdDeducciones == id);
                var modelo = new EditarDeduccionesViewModel()
                {
                   IdDeducciones= query.IdDeducciones,
                   Descripcion = query.Descripcion,

                };
                return PartialView(modelo);
            }
        }

        // POST: Deducciones/Edit/5
        [HttpPost]
        public ActionResult Edit(EditarDeduccionesViewModel modelo)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new {Estatu = false, Mensaje = "Revisar campos requeridos."},
                        JsonRequestBehavior.AllowGet);
                }
                using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
                {
                    var query = dbRecursosHumanosEntities.Deducciones.FirstOrDefault(x =>
                        x.IdDeducciones == modelo.IdDeducciones);
                    query.Descripcion = modelo.Descripcion;

                    dbRecursosHumanosEntities.SaveChanges();

                    TempData["MensajeDeRespuesta"] = new RespuestaBase
                    {
                        Estatus = true,
                        Mensage = "Se ha modificado con exito.",
                        Titulo = "Deducciones"
                    };
                    return Json(new {Estatu = true, Mensaje = "Se ha modificado con exito."},
                        JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new {Estatus = false, Mensaje = ex.Message},
                    JsonRequestBehavior.AllowGet);
            }
        }

        // GET: Deducciones/Delete/5
            public ActionResult Delete(int id)
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {

                var query = dbRecursosHumanosEntities.Deducciones.FirstOrDefault(x => x.IdDeducciones == id);

                var modelo = new EditarDeduccionesViewModel
                {


                    EstadoDeducciones = query.EstadoDeducciones = false,

                };
                dbRecursosHumanosEntities.SaveChanges();
                TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha modificado con exito.", Titulo = "Deducciones" };
                return Json(new { Estatu = true, Mensaje = "Se ha modificado con exito." },
                    JsonRequestBehavior.AllowGet);

            }
        }

        // POST: Deducciones/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SmartAdminMvc.Database;
using SmartAdminMvc.Models.Areas;
using SmartAdminMvc.Models.Base;
using SmartAdminMvc.Models.Genero;

namespace SmartAdminMvc.Controllers
{
    [Authorize]
    public class TipoGeneroController : Controller
    {
        // GET: TipoGenero
        public ActionResult Index()
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {
                var listadoGenero = dbRecursosHumanosEntities.TipoGeneroes.Select(x => 
                new ListarGeneroViewModel
                {
                    IdGenero = x.IdTipoGenero,
                    Genero = x.Descripcion,
                    EstadoGenero = x.EstadoTipoGenero
                }).ToList();
                return View(listadoGenero);
            }
        }

        // GET: TipoGenero/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: TipoGenero/Create
        public ActionResult Create()
        {
            return PartialView(new CrearGeneroViewModel());
        }

        // POST: TipoGenero/Create
        [HttpPost]
        public JsonResult Create(CrearGeneroViewModel model)
        {
           
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Estatus = false, Mensaje = "Revisar campos requeridos." },
                        JsonRequestBehavior.AllowGet);
                }

                using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
                {

                    var nuevoGenero = new TipoGenero
                    {
                       Descripcion = model.Genero,
                       EstadoTipoGenero = true,
                        CreadoPor = User.Identity.Name,
                        ModificadoPor = User.Identity.Name,
                        FechaCreacion = DateTime.Now,
                        FechaModificado = DateTime.Now

                    };
                    dbRecursosHumanosEntities.TipoGeneroes.Add(nuevoGenero);
                    dbRecursosHumanosEntities.SaveChanges();

                    TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha guardado de manera exitosa.", Titulo = "Tipo Generos" };
                    return Json(new { Estatus = true, Mensaje = "Se ha  guardado con exito." },
                        JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {

                return Json(new { Estatus = false, Mensaje = ex.Message },
                    JsonRequestBehavior.AllowGet);

            }
        }
        // GET: TipoGenero/Edit/5
        public ActionResult Edit(int id)
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {
               var query = dbRecursosHumanosEntities.TipoGeneroes.FirstOrDefault(x => x.IdTipoGenero == id);
                var modelo = new EditarGeneroViewModel
                {
                    IdGenero = query.IdTipoGenero,
                    Genero = query.Descripcion,
                
                };
                return PartialView(modelo);
            }
        }

        // POST: TipoGenero/Edit/5
        [HttpPost]
        public ActionResult Edit(EditarGeneroViewModel modelo)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Estatu = false, Mensaje = "Revisar campos requeridos." },
                        JsonRequestBehavior.AllowGet);
                }
                using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
                {
                    var query = dbRecursosHumanosEntities.TipoGeneroes.FirstOrDefault(x => x.IdTipoGenero == modelo.IdGenero);
                    query.Descripcion = modelo.Genero;
                    
                    dbRecursosHumanosEntities.SaveChanges();

                    TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha modificado con exito.", Titulo = "Tipo Generos" };
                    return Json(new { Estatu = true, Mensaje = "Se ha modificado con exito." },
                        JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Estatus = false, Mensaje = ex.Message },
                    JsonRequestBehavior.AllowGet);
            }
        }

        // GET: TipoGenero/Delete/5
        public ActionResult Delete(int id)
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {

                var query = dbRecursosHumanosEntities.TipoGeneroes.FirstOrDefault(x => x.IdTipoGenero == id);

                var modelo = new EditarGeneroViewModel
                {


                    EstadoGenero = query.EstadoTipoGenero = false,

                };
                dbRecursosHumanosEntities.SaveChanges();
                TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha modificado con exito.", Titulo = "Genero" };
                return Json(new { Estatu = true, Mensaje = "Se ha modificado con exito." },
                    JsonRequestBehavior.AllowGet);

            }
        }

        // POST: TipoGenero/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

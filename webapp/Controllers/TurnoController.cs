﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SmartAdminMvc.Database;
using SmartAdminMvc.Models.Base;
using SmartAdminMvc.Models.Genero;
using SmartAdminMvc.Models.Turno;

namespace SmartAdminMvc.Controllers
{
    [Authorize]
    public class TurnoController : Controller
    {
        // GET: Turno
        public ActionResult Index()
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {
                var listadoTurno = dbRecursosHumanosEntities.Turnoes.Select(x => new ListarTurnoViewModel() { IdTurno = x.IdTurno, Nombre = x.Descripcion, Estado = x.EstadoTurno }).ToList();
                return View(listadoTurno);
            }
        }

        // GET: Turno/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Turno/Create
        public ActionResult Create()
        {

            return PartialView(new CrearTurnoViewModel());
        }
        

        // POST: Turno/Create
        [HttpPost]
        public JsonResult Create(CrearTurnoViewModel modelo)
        {

            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Estatus = false, Mensaje = "Revisar campos requeridos." },
                        JsonRequestBehavior.AllowGet);
                }

                using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
                {
                    var nuevoTur = new Turno
                    {
                        Descripcion = modelo.Nombre,
                        EstadoTurno = true,
                        CreadoPor = User.Identity.Name,
                        ModificadoPor = User.Identity.Name,
                        FechaCreacion = DateTime.Now,
                        FechaModificado = DateTime.Now

                    };
                    dbRecursosHumanosEntities.Turnoes.Add(nuevoTur);
                    dbRecursosHumanosEntities.SaveChanges();

                    TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha guardado de manera exitosa.", Titulo = "Turno" };
                    return Json(new { Estatus = true, Mensaje = "Se ha  guardado con exito." },
                        JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {

                return Json(new { Estatus = false, Mensaje = ex.Message },
                    JsonRequestBehavior.AllowGet);

            }
        }

        // GET: Turno/Edit/5
        public ActionResult Edit(int id)
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {
                var query = dbRecursosHumanosEntities.Turnoes.FirstOrDefault(x => x.IdTurno == id);
                var modelo = new EditarTurnoViewModel()
                {
                    IdTurno = query.IdTurno,
                    Nombre = query.Descripcion,

                };
                return PartialView(modelo);
            }
        }

        // POST: Turno/Edit/5
        [HttpPost]
        public ActionResult Edit(EditarTurnoViewModel modelo)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Estatu = false, Mensaje = "Revisar campos requeridos." },
                        JsonRequestBehavior.AllowGet);
                }
                using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
                {
                    var query = dbRecursosHumanosEntities.Turnoes.FirstOrDefault(x => x.IdTurno == modelo.IdTurno);
                    query.Descripcion = modelo.Nombre;

                    dbRecursosHumanosEntities.SaveChanges();

                    TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha modificado con exito.", Titulo = "Turno" };
                    return Json(new { Estatu = true, Mensaje = "Se ha modificado con exito." },
                        JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Estatus = false, Mensaje = ex.Message },
                    JsonRequestBehavior.AllowGet);
            }
        }

        // GET: Turno/Delete/5
        public ActionResult Delete(int id)
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {

                var query = dbRecursosHumanosEntities.Turnoes.FirstOrDefault(x => x.IdTurno == id);

                var modelo = new EditarTurnoViewModel
                {


                    Estado = query.EstadoTurno = false,

                };
                dbRecursosHumanosEntities.SaveChanges();
                TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha modificado con exito.", Titulo = "Turnos" };
                return Json(new { Estatu = true, Mensaje = "Se ha modificado con exito." },
                    JsonRequestBehavior.AllowGet);

            }
        }

        // POST: Turno/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

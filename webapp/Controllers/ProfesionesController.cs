﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SmartAdminMvc.Database;
using SmartAdminMvc.Models.Areas;
using SmartAdminMvc.Models.Profesiones;
using SmartAdminMvc.Controllers;
using SmartAdminMvc.Models.Base;

namespace SmartAdminMvc.Controllers
{
    [Authorize]
    public class ProfesionesController : Controller
    {
        // GET: Profesiones
        public ActionResult Index()
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {
                var listadoProfesiones = dbRecursosHumanosEntities.Profesiones.Select(x => new ListarProfesionesViewModel { IdProfesione = x.IdProfesiones, NivelEducativo = x.NivelEducativo.Descripcion, Descripcion = x.Descripcion, Estado = x.EstadoProfesiones }).ToList();
                return View(listadoProfesiones);
            }
        }

        // GET: Profesiones/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Profesiones/Create
        public ActionResult Create()
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {
                ViewBag.SelectNivelEducativo = dbRecursosHumanosEntities.NivelEducativoes.Where(x=>x.EstadoNivelEducativo==true).ToList()
                    .Select(x => new SelectListItem
                    {
                        Text = x.Descripcion,
                        Value = x.IdNivelEducativo.ToString()
                    }).ToList();

                return PartialView(new CrearProfesionesViewModel());
            }
        }

        // POST: Profesiones/Create
        [HttpPost]
        public JsonResult Create(CrearProfesionesViewModel modelo)
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {
               
                try
                {
                    if (!ModelState.IsValid)
                    {
                        return Json(new { Estatus = false, Mensaje = "Revisar campos requeridos." },
                            JsonRequestBehavior.AllowGet);
                    }
                    var nuevaProfe = new Profesione
                    {
                        IdNivelEducativ = modelo.IdNivelEducativo,
                        Descripcion = modelo.Descripcion,
                        CreadoPor = User.Identity.Name,
                        ModificadoPor = User.Identity.Name,
                        EstadoProfesiones = true,
                        FechaCreacion = DateTime.Now,
                        FechaModificado = DateTime.Now
                    };
                    var condiPro = dbRecursosHumanosEntities.Profesiones.Any(x => x.Descripcion == modelo.Descripcion && x.IdNivelEducativ == modelo.IdNivelEducativo);
                    if (condiPro == true)
                    {
                        return Json(new { Estatus = false, Mensaje = "Nombre de Area ya Existente." },
                            JsonRequestBehavior.AllowGet);
                    }
                    dbRecursosHumanosEntities.Profesiones.Add(nuevaProfe);

                    dbRecursosHumanosEntities.SaveChanges();

                    TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha guardado de manera exitosa.", Titulo = "Profesiones" };
                    return Json(new { Estatus = true, Mensaje = "Se ha  guardado con exito." },
                        JsonRequestBehavior.AllowGet);

                }
                catch (Exception ex)
                {

                    return Json(new { Estatus = false, Mensaje = ex.Message },
                        JsonRequestBehavior.AllowGet);

                }
            }
        }

        // GET: Profesiones/Edit/5
        public ActionResult Edit(int id)
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {
                ViewBag.SelectNivelEducativo = dbRecursosHumanosEntities.NivelEducativoes.ToList()
                    .Select(x => new SelectListItem
                    {
                        Text = x.Descripcion,
                        Value = x.IdNivelEducativo.ToString()
                    }).ToList();
                var query = dbRecursosHumanosEntities.Profesiones.FirstOrDefault(x => x.IdProfesiones == id);
                var modelo = new EditarProfesionesViewModel()
                {

                    IdNivelEducativo = (int)query.IdNivelEducativ,

                    IdProfesione= query.IdProfesiones,

                    Descripcion = query.Descripcion,

                };

                return PartialView(modelo);
            }
        }

        // POST: Profesiones/Edit/5
        [HttpPost]
        public ActionResult Edit(EditarProfesionesViewModel modelo)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Estatu = false, Mensaje = "Revisar campos requeridos." },
                        JsonRequestBehavior.AllowGet);
                }
                using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
                {

                    var query = dbRecursosHumanosEntities.Profesiones.FirstOrDefault(x => x.IdProfesiones == modelo.IdProfesione);
                    var condiPro = dbRecursosHumanosEntities.Profesiones.Any(x => x.Descripcion == modelo.Descripcion && x.IdNivelEducativ == modelo.IdNivelEducativo);

                    if (condiPro == true)
                    {
                        return Json(new { Estatus = false, Mensaje = "Nombre de Area ya Existente." },
                            JsonRequestBehavior.AllowGet);
                    }
                    query.IdNivelEducativ = modelo.IdNivelEducativo;

                    query.Descripcion = modelo.Descripcion;

                    dbRecursosHumanosEntities.SaveChanges();

                    TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha modificado con exito.", Titulo = "Profesiones" };
                    return Json(new { Estatu = true, Mensaje = "Se ha modificado con exito." },
                        JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                return Json(new { Estatus = false, Mensaje = ex.Message },
                    JsonRequestBehavior.AllowGet);
            }
        }

        // GET: Profesiones/Delete/5
        public ActionResult Delete(int id)
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {

                var query = dbRecursosHumanosEntities.Profesiones.FirstOrDefault(x => x.IdProfesiones == id);

                var modelo = new EditarProfesionesViewModel
                {


                    Estado = query.EstadoProfesiones = false,

                };
                dbRecursosHumanosEntities.SaveChanges();
                TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha modificado con exito.", Titulo = "Profesiones" };
                return Json(new { Estatu = true, Mensaje = "Se ha modificado con exito." },
                    JsonRequestBehavior.AllowGet);

            }
        }

        // POST: Profesiones/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

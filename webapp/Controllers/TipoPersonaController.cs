﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SmartAdminMvc.Database;
using SmartAdminMvc.Models.Base;
using SmartAdminMvc.Models.TipoPersona;


namespace SmartAdminMvc.Controllers
{
    [Authorize]
    public class TipoPersonaController : Controller
    {
        // GET: TipoPersona
        public ActionResult Index()
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {
                var listadoTipoPlanilla = dbRecursosHumanosEntities.TipoPersonas.Select(x => new TipoPersonaViewModel { idTipoPersona = x.IdTipoPersona, Nombre= x.Descripcion, Estado = x.EstadoTipoPersona }).ToList();
                return View(listadoTipoPlanilla);
            }
        }

        // GET: TipoPersona/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: TipoPersona/Create
        public ActionResult Create()
        {
            return PartialView(new CreaeTipoPersonaViewModel());
        }

        // POST: TipoPersona/Create
        [HttpPost]
        public JsonResult Create(CreaeTipoPersonaViewModel modelo)
        {

            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Estatus = false, Mensaje = "Revisar campos requeridos." },
                        JsonRequestBehavior.AllowGet);
                }

                using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
                {
                    var nuevoTp = new TipoPersona()
                    {
                        Descripcion = modelo.Nombre,
                        EstadoTipoPersona = true,
                        CreadoPor = User.Identity.Name,
                        ModificadoPor = User.Identity.Name,
                        FechaCreacion = DateTime.Now,
                        FechaModificado = DateTime.Now

                    };
                    dbRecursosHumanosEntities.TipoPersonas.Add(nuevoTp);
                    dbRecursosHumanosEntities.SaveChanges();

                    TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha guardado de manera exitosa.", Titulo = "Tipo Persona" };
                    return Json(new { Estatus = true, Mensaje = "Se ha  guardado con exito." },
                        JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {

                return Json(new { Estatus = false, Mensaje = ex.Message },
                    JsonRequestBehavior.AllowGet);

            }
        }

        // GET: TipoPersona/Edit/5
        public ActionResult Edit(int id)
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {
                var query = dbRecursosHumanosEntities.TipoPersonas.FirstOrDefault(x => x.IdTipoPersona == id);
                var modelo = new EditarPersonaViewModel
                {
                    idTipoPersona = query.IdTipoPersona,
                    Nombre= query.Descripcion,

                };
                return PartialView(modelo);
            }
        }

        // POST: TipoPersona/Edit/5
        [HttpPost]
        public ActionResult Edit(EditarPersonaViewModel modelo)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Estatu = false, Mensaje = "Revisar campos requeridos." },
                        JsonRequestBehavior.AllowGet);
                }
                using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
                {
                    var query = dbRecursosHumanosEntities.TipoPersonas.FirstOrDefault(x => x.IdTipoPersona == modelo.idTipoPersona);
                    query.Descripcion = modelo.Nombre;

                    dbRecursosHumanosEntities.SaveChanges();

                    TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha modificado con exito.", Titulo = "Tipo Persona" };
                    return Json(new { Estatu = true, Mensaje = "Se ha modificado con exito." },
                        JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Estatus = false, Mensaje = ex.Message },
                    JsonRequestBehavior.AllowGet);
            }
        }

        // GET: TipoPersona/Delete/5
        public ActionResult Delete(int id)
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {

                var query = dbRecursosHumanosEntities.TipoPersonas.FirstOrDefault(x => x.IdTipoPersona == id);

                var modelo = new EditarPersonaViewModel
                {


                    Estado = query.EstadoTipoPersona = false,

                };
                dbRecursosHumanosEntities.SaveChanges();
                TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha modificado con exito.", Titulo = "Tipo Persona" };
                return Json(new { Estatu = true, Mensaje = "Se ha modificado con exito." },
                    JsonRequestBehavior.AllowGet);

            }
        }

        // POST: TipoPersona/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

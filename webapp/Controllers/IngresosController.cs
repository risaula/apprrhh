﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SmartAdminMvc.Database;
using SmartAdminMvc.Models.Base;
using SmartAdminMvc.Models.Deducciones;
using SmartAdminMvc.Models.Idiomas;
using SmartAdminMvc.Models.Ingresos;

namespace SmartAdminMvc.Controllers
{
    [Authorize]
    public class IngresosController : Controller
    {
        // GET: Ingresos
        public ActionResult Index()
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {
                var ListarDed = dbRecursosHumanosEntities.Ingresos.Select(
                    x => new ListarIngresosViewModel
                    {
                        IdIngresos= x.IdIngresos,
                       Descripcion = x.Descripcion,
                        EstadoIngresos = x.EstadoIngresos
                    }).ToList();
                return View(ListarDed);
            }
        }

        // GET: Ingresos/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Ingresos/Create
        public ActionResult Create()
        {
            return PartialView(new CrearIngresoViewModel());
        }

        // POST: Ingresos/Create
        [HttpPost]
        public JsonResult Create(CrearIngresoViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new {Estatus = false, Mensaje = "Revisar campos requeridos."},
                        JsonRequestBehavior.AllowGet);
                }

                using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
                {
                    var nuevoIng = new Ingreso
                    {
                        Descripcion = model.Descripcion,
                        EstadoIngresos = true,
                        CreadoPor = User.Identity.Name,
                        ModificadoPor = User.Identity.Name,
                        FechaCreacion = DateTime.Now,
                        FechaModificado = DateTime.Now

                    };
                    dbRecursosHumanosEntities.Ingresos.Add(nuevoIng);
                    dbRecursosHumanosEntities.SaveChanges();

                    TempData["MensajeDeRespuesta"] = new RespuestaBase
                    {
                        Estatus = true,
                        Mensage = "Se ha guardado de manera exitosa.",
                        Titulo = "Ingresos"
                    };
                    return Json(new {Estatus = true, Mensaje = "Se ha  guardado con exito."},
                        JsonRequestBehavior.AllowGet);


                }
            }
            catch (Exception ex)
            {

                return Json(new {Estatus = false, Mensaje = ex.Message},
                    JsonRequestBehavior.AllowGet);

            }
        }

        // GET: Ingresos/Edit/5
            public ActionResult Edit(int id)
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {
                var query = dbRecursosHumanosEntities.Ingresos.FirstOrDefault(x => x.IdIngresos == id);
                var modelo = new EditarIngresosViewModel()
                {
                    IdIngresos = query.IdIngresos,
                    Descripcion = query.Descripcion,

                };
                return PartialView(modelo);
            }
        }

        // POST: Ingresos/Edit/5
        [HttpPost]
        public ActionResult Edit(EditarIngresosViewModel modelo)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Estatu = false, Mensaje = "Revisar campos requeridos." },
                        JsonRequestBehavior.AllowGet);
                }
                using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
                {
                    var query = dbRecursosHumanosEntities.Ingresos.FirstOrDefault(x =>
                        x.IdIngresos == modelo.IdIngresos);
                    query.Descripcion = modelo.Descripcion;

                    dbRecursosHumanosEntities.SaveChanges();

                    TempData["MensajeDeRespuesta"] = new RespuestaBase
                    {
                        Estatus = true,
                        Mensage = "Se ha modificado con exito.",
                        Titulo = "Ingresos"
                    };
                    return Json(new { Estatu = true, Mensaje = "Se ha modificado con exito." },
                        JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Estatus = false, Mensaje = ex.Message },
                    JsonRequestBehavior.AllowGet);
            }
        }

        // GET: Ingresos/Delete/5
        public ActionResult Delete(int id)
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {

                var query = dbRecursosHumanosEntities.Ingresos.FirstOrDefault(x => x.IdIngresos == id);

                var modelo = new EditarIngresosViewModel
                {


                    EstadoIngresos = query.EstadoIngresos = false,

                };
                dbRecursosHumanosEntities.SaveChanges();
                TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha modificado con exito.", Titulo = "Ingresos" };
                return Json(new { Estatu = true, Mensaje = "Se ha modificado con exito." },
                    JsonRequestBehavior.AllowGet);

            }
        }

        // POST: Ingresos/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

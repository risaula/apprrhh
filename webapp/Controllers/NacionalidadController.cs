﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using SmartAdminMvc.Database;
using SmartAdminMvc.Models.Base;
using SmartAdminMvc.Models.Idiomas;
using SmartAdminMvc.Models.Nacionalidad;

namespace SmartAdminMvc.Controllers
{
    [Authorize]
    public class NacionalidadController : Controller
    {
        // GET: Nacionalidad
        public ActionResult Index()
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {
                var ListarNacionalidad = dbRecursosHumanosEntities.Nacionalidads.Select(x => new ListarNacionalidadViewModel{ IdNac= x.IdNacionalidad, Nacionalidad = x.Descripcion, NumeroIdentidad = x.FormatoIdentidad, Telefono = x.FormatoTelefonico, Estado = x.EstadoNacionalidad }).ToList();
                return View(ListarNacionalidad);
            }
        }

        // GET: Nacionalidad/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Nacionalidad/Create
        public ActionResult Create()
        {
            return PartialView(new CrearNacionalidadViewModel());
        }

        // POST: Nacionalidad/Create
        [HttpPost]
        public JsonResult Create(CrearNacionalidadViewModel modelo)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Estatus = false, Mensaje = "Revisar campos requeridos." },
                        JsonRequestBehavior.AllowGet);
                }

                using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
                {

                    var nuevaNac = new Nacionalidad
                    {
                        
                        Descripcion = modelo.Nacionalidad,
                        FormatoTelefonico = modelo.Telefono,
                        FormatoIdentidad = modelo.NumeroIdentidad,
                        CreadoPor = User.Identity.Name,
                        ModificadoPor = User.Identity.Name,
                        EstadoNacionalidad = true,
                        FechaCreacion = DateTime.Now,
                        FechaModificado = DateTime.Now
                    };
                    dbRecursosHumanosEntities.Nacionalidads.Add(nuevaNac);
                    dbRecursosHumanosEntities.SaveChanges();

                    TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha guardado de manera exitosa.", Titulo = "Nacionalidad" };
                    return Json(new { Estatus = true, Mensaje = "Se ha  guardado con exito." },
                        JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                return Json(new { Estatus = false, Mensaje = ex.Message },
                    JsonRequestBehavior.AllowGet);

            }
        }

        // GET: Nacionalidad/Edit/5
        public ActionResult Edit(int id)
        {
            using(var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {
                var query = dbRecursosHumanosEntities.Nacionalidads.FirstOrDefault(x => x.IdNacionalidad == id);
                var modelo = new EditarNacionalidadViewModel
                {

                   
                    IdNac = query.IdNacionalidad,
                    Nacionalidad = query.Descripcion,
                   NumeroIdentidad = query.FormatoIdentidad,
                    Telefono = query.FormatoTelefonico,

                };

                return PartialView(modelo);
            }
        }

        // POST: Nacionalidad/Edit/5
        [HttpPost]
        public ActionResult Edit(EditarNacionalidadViewModel modelo)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Estatu = false, Mensaje = "Revisar campos requeridos." },
                        JsonRequestBehavior.AllowGet);
                }
                using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
                {
                    var query = dbRecursosHumanosEntities.Nacionalidads.FirstOrDefault(x => x.IdNacionalidad == modelo.IdNac);
                    query.Descripcion = modelo.Nacionalidad;
                    query.FormatoTelefonico = modelo.Telefono;
                    query.FormatoIdentidad = modelo.NumeroIdentidad;

                    dbRecursosHumanosEntities.SaveChanges();

                    TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha modificado con exito.", Titulo = "Nacionalidad" };
                    return Json(new { Estatu = true, Mensaje = "Se ha modificado con exito." },
                        JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Estatus = false, Mensaje = ex.Message },
                    JsonRequestBehavior.AllowGet);
            }
        }

        // GET: Nacionalidad/Delete/5
        public ActionResult Delete(int id)
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {

                var query = dbRecursosHumanosEntities.Nacionalidads.FirstOrDefault(x => x.IdNacionalidad == id);

                var modelo = new EditarNacionalidadViewModel
                {


                    Estado = query.EstadoNacionalidad = false,

                };
                dbRecursosHumanosEntities.SaveChanges();
                TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha modificado con exito.", Titulo = "Nacionalidad" };
                return Json(new { Estatu = true, Mensaje = "Se ha modificado con exito." },
                    JsonRequestBehavior.AllowGet);

            }
        }

        // POST: Nacionalidad/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

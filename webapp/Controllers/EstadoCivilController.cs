﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SmartAdminMvc.Database;
using SmartAdminMvc.Models.Areas;
using SmartAdminMvc.Models.EstadoCivil;
using SmartAdminMvc.Models.Idiomas;
using SmartAdminMvc.Models.Base;

namespace SmartAdminMvc.Controllers
{
    [Authorize]
    public class EstadoCivilController : Controller
    {
        // GET: EstadoCivil
        public ActionResult Index()
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {
                var ListarEstadoCivil = dbRecursosHumanosEntities.EstadoCivils.Select(x => 
                new ListarEstadoCivilViewModel
                {
                    IdEstadoC = x.IdEstadoCivil,
                    EstadoCivil = x.Descripcion,
                    EstadoEstadoCivil = x.EstadoEstadoCivil
                    }).ToList();
                return View(ListarEstadoCivil);
            }
        }

        // GET: EstadoCivil/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: EstadoCivil/Create
        public ActionResult Create()
        {
            return PartialView(new CrearEstadoCivilViewModel());
        }

        // POST: EstadoCivil/Create
        [HttpPost]
        public JsonResult Create(CrearEstadoCivilViewModel modelo)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Estatus = false, Mensaje = "Revisar campos requeridos." },
                        JsonRequestBehavior.AllowGet);
                }

                using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
                {
                    var nuevoEstadoCivil = new EstadoCivil
                    {
                        Descripcion = modelo.EstadoCivil,
                        EstadoEstadoCivil = true,
                        CreadoPor = User.Identity.Name,
                        ModificadoPor = User.Identity.Name,
                        FechaCreacion = DateTime.Now,
                        FechaModificado = DateTime.Now
                        
                    };

                    dbRecursosHumanosEntities.EstadoCivils.Add(nuevoEstadoCivil);
                    dbRecursosHumanosEntities.SaveChanges();

                    TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha guardado de manera exitosa.", Titulo = "Estado Civil" };
                    return Json(new { Estatus = true, Mensaje = "Se ha  guardado con exito." },
                        JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {

                return Json(new { Estatus = false, Mensaje = ex.Message },
                    JsonRequestBehavior.AllowGet);

            }
        }

        // GET: EstadoCivil/Edit/5
        public ActionResult Edit(int id)
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {
                var query = dbRecursosHumanosEntities.EstadoCivils.FirstOrDefault(x => x.IdEstadoCivil== id);
                var modelo = new EditarEstadoCivilViewModel
                {

                   
                    IdEstadoC= query.IdEstadoCivil,

                    EstadoCivil = query.Descripcion,

                    
                };
                return PartialView(modelo);
            }
        }

        // POST: EstadoCivil/Edit/5
        [HttpPost]
        public ActionResult Edit(EditarEstadoCivilViewModel modelo)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Estatu = false, Mensaje = "Revisar campos requeridos." },
                        JsonRequestBehavior.AllowGet);
                }
                using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
                {
                    var query = dbRecursosHumanosEntities.EstadoCivils.FirstOrDefault(x => x.IdEstadoCivil == modelo.IdEstadoC);

                    query.Descripcion = modelo.EstadoCivil;
                    
                   dbRecursosHumanosEntities.SaveChanges();

                    TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha modificado con exito.", Titulo = "Estado Civil" };
                    return Json(new { Estatu = true, Mensaje = "Se ha modificado con exito." },
                        JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Estatus = false, Mensaje = ex.Message },
                    JsonRequestBehavior.AllowGet);
            }
        }

        // GET: EstadoCivil/Delete/5
        public ActionResult Delete(int id)
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {

                var query = dbRecursosHumanosEntities.EstadoCivils.FirstOrDefault(x => x.IdEstadoCivil == id);

                var modelo = new EditarEstadoCivilViewModel
                {


                    EstadoEstadoCivil = query.EstadoEstadoCivil = false,

                };
                dbRecursosHumanosEntities.SaveChanges();
                TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha modificado con exito.", Titulo = "Estado Civil" };
                return Json(new { Estatu = true, Mensaje = "Se ha modificado con exito." },
                    JsonRequestBehavior.AllowGet);

            }
        }

        // POST: EstadoCivil/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SmartAdminMvc.Database;
using SmartAdminMvc.Models.Base;
using SmartAdminMvc.Models.Genero;
using SmartAdminMvc.Models.TipoPlanilla;
using SmartAdminMvc.Models.Turno;

namespace SmartAdminMvc.Controllers
{
    [Authorize]
    public class TipoPlanillaController : Controller
    {
        // GET: TipoPlanilla
        public ActionResult Index()
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {
                var listadoTipoPlanilla = dbRecursosHumanosEntities.TipoPlanillas.Select(x => new ListarTipoPlanillaViewModel { idTipoPlanilla = x.IdTipoPlanilla, Nombre = x.Descripcion, Estado = x.EstadoTipoPlanilla }).ToList();
                return View(listadoTipoPlanilla);
            }
        }

        // GET: TipoPlanilla/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: TipoPlanilla/Create
        public ActionResult Create()
        {
            return PartialView(new CrearTipoPlanillaViewModel());
        }

        // POST: TipoPlanilla/Create
        [HttpPost]
        public JsonResult Create(CrearTipoPlanillaViewModel modelo)
        {

            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Estatus = false, Mensaje = "Revisar campos requeridos." },
                        JsonRequestBehavior.AllowGet);
                }

                using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
                {
                    var nuevoPla = new TipoPlanilla
                    {
                        Descripcion = modelo.Nombre,
                        EstadoTipoPlanilla = true,
                        CreadoPor = User.Identity.Name,
                        ModificadoPor = User.Identity.Name,
                        FechaCreacion = DateTime.Now,
                        FechaModificado = DateTime.Now

                    };
                    dbRecursosHumanosEntities.TipoPlanillas.Add(nuevoPla);
                    dbRecursosHumanosEntities.SaveChanges();

                    TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha guardado de manera exitosa.", Titulo = "Tipo Planilla" };
                    return Json(new { Estatus = true, Mensaje = "Se ha  guardado con exito." },
                        JsonRequestBehavior.AllowGet);


                }
            }
            catch (Exception ex)
            {

                return Json(new { Estatus = false, Mensaje = ex.Message },
                    JsonRequestBehavior.AllowGet);

            }
        }
        // GET: TipoPlanilla/Edit/5
        public ActionResult Edit(int id)
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {
                var query = dbRecursosHumanosEntities.TipoPlanillas.FirstOrDefault(x => x.IdTipoPlanilla == id);
                var modelo = new EditarTipoPlanillaViewModel
                {
                    idTipoPlanilla = query.IdTipoPlanilla,
                    Nombre = query.Descripcion,

                };
                return PartialView(modelo);
            }
        }

        // POST: TipoPlanilla/Edit/5
        [HttpPost]
        public ActionResult Edit(EditarTipoPlanillaViewModel modelo)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Estatu = false, Mensaje = "Revisar campos requeridos." },
                        JsonRequestBehavior.AllowGet);
                }
                using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
                {
                    var query = dbRecursosHumanosEntities.TipoPlanillas.FirstOrDefault(x => x.IdTipoPlanilla == modelo.idTipoPlanilla);
                    query.Descripcion = modelo.Nombre;

                    dbRecursosHumanosEntities.SaveChanges();

                    TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha modificado con exito.", Titulo = "Tipo Planilla" };
                    return Json(new { Estatu = true, Mensaje = "Se ha modificado con exito." },
                        JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Estatus = false, Mensaje = ex.Message },
                    JsonRequestBehavior.AllowGet);
            }
        }

        // GET: TipoPlanilla/Delete/5
        public ActionResult Delete(int id)
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {

                var query = dbRecursosHumanosEntities.TipoPlanillas.FirstOrDefault(x => x.IdTipoPlanilla == id);

                var modelo = new EditarTipoPlanillaViewModel
                {


                    Estado = query.EstadoTipoPlanilla = false,

                };
                dbRecursosHumanosEntities.SaveChanges();
                TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha modificado con exito.", Titulo = "Tipo Planilla" };
                return Json(new { Estatu = true, Mensaje = "Se ha modificado con exito." },
                    JsonRequestBehavior.AllowGet);

            }
        }

        // POST: TipoPlanilla/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

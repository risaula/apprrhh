﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartAdminMvc.Controllers
{
    public class GetCambiarStatusCandidatoController : Controller
    {
        // GET: GetCambiarStatusCandidato
        public ActionResult Index()
        {
            return View();
        }

        // GET: GetCambiarStatusCandidato/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: GetCambiarStatusCandidato/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: GetCambiarStatusCandidato/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: GetCambiarStatusCandidato/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: GetCambiarStatusCandidato/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: GetCambiarStatusCandidato/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: GetCambiarStatusCandidato/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

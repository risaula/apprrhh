﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SmartAdminMvc.Database;
using SmartAdminMvc.Models.Areas;
using SmartAdminMvc.Models.Base;
using SmartAdminMvc.Models.Observaciones;

namespace SmartAdminMvc.Controllers
{
    [Authorize]
    public class ObservacionesController : Controller
    {
        public ActionResult Index()
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {
                //  var ListarPersona = dbRecursosHumanosEntities.Personas.Where(x => x.IdTipoPersona == 1).Select(x => new CrearCandidatoViewModel
                //var listadoDeAreas = dbRecursosHumanosEntities.Areas.Where(x=>x.EstadoArea==true  ).Select(x => new ListarAreasViewModel
                var ListarAreas = dbRecursosHumanosEntities.Observaciones.Select(x =>
                    new ListarObservaciones
                    {
                        IdObservacion= x.IdObservacion,
                        Descripcion = x.Descripcion,
                       Personas= x.Persona.Nombre,
                        Estado = x.Estado,
                      }).ToList();
                return View(ListarAreas);
            }
        }


        // GET: Area/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Area/Create
        public ActionResult Create()
        {

            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {

                ViewBag.SelectPersonas = dbRecursosHumanosEntities.Personas.ToList()
                    .Select(x => new SelectListItem
                    {
                        Text = x.Nombre,
                        Value = x.IdPersonas.ToString()
                    }).ToList();

                return PartialView(new CrearObservacionesViewModel());

            }

        }

        // POST: Persona/Create
        [HttpPost]
        public JsonResult Create(CrearObservacionesViewModel modelo)
        {

            try
            {

                if (!ModelState.IsValid)
                {
                    return Json(new { Estatus = false, Mensaje = "Revisar campos requeridos." },
                        JsonRequestBehavior.AllowGet);
                }

                using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
                {

                    var nuevaArea = new Observacione
                    {
                        IdPersona = modelo.IdPersona,
                        Descripcion = modelo.Descripcion,
                        CreadoPor = User.Identity.Name,
                        ModificadoPor = User.Identity.Name,
                        Estado = true,
                        FechaCreacion = DateTime.Now,
                        FechaModificacion = DateTime.Now,
                        
                    };

                                  
                    dbRecursosHumanosEntities.Observaciones.Add(nuevaArea);

                    dbRecursosHumanosEntities.SaveChanges();

                    TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha guardado de manera exitosa.", Titulo = "Observaciones" };
                    return Json(new { Estatus = true, Mensaje = "Se ha  guardado con exito." },
                        JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {

                return Json(new { Estatus = false, Mensaje = ex.Message },
                    JsonRequestBehavior.AllowGet);

            }

        }


        // GET: Area/Edit/5
        public ActionResult Edit(int id)
        {

            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {

                ViewBag.SelectPersonas = dbRecursosHumanosEntities.Personas.ToList()
                    .Select(x => new SelectListItem
                    {
                        Text = x.Nombre,
                        Value = x.IdPersonas.ToString()
                    }).ToList();

                var query = dbRecursosHumanosEntities.Observaciones.FirstOrDefault(x => x.IdObservacion == id);

                var modelo = new EditarObservacionesViewModel
                {

                    IdPersona = (int)query.IdPersona,

                    IdObservacion= query.IdObservacion,

                    Descripcion = query.Descripcion,

                };

                return PartialView(modelo);

            }

        }

        // POST: Persona/Edit/5
        [HttpPost]
        public ActionResult Edit(EditarObservacionesViewModel modelo)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Estatu = false, Mensaje = "Revisar campos requeridos." },
                        JsonRequestBehavior.AllowGet);
                }
                using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
                {

                    var query = dbRecursosHumanosEntities.Observaciones.FirstOrDefault(x => x.IdObservacion == modelo.IdObservacion);
                    var condiArea = dbRecursosHumanosEntities.Observaciones.Any(x => x.Descripcion == modelo.Descripcion && x.IdPersona == modelo.IdPersona);

                    if (condiArea == true)
                    {
                        return Json(new { Estatus = false, Mensaje = "Nombre de Area ya Existente." },
                            JsonRequestBehavior.AllowGet);
                    }
                    query.IdPersona= modelo.IdPersona;

                    query.Descripcion = modelo.Descripcion;

                    dbRecursosHumanosEntities.SaveChanges();

                    TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha modificado con exito.", Titulo = "Areas" };
                    return Json(new { Estatu = true, Mensaje = "Se ha modificado con exito." },
                        JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                return Json(new { Estatus = false, Mensaje = ex.Message },
                    JsonRequestBehavior.AllowGet);
            }
        }
        // GET: Area/Delete/5
        public ActionResult Delete(int id)
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {

                var query = dbRecursosHumanosEntities.Observaciones.FirstOrDefault(x => x.IdObservacion == id);

                var modelo = new EditarObservacionesViewModel
                {


                    Estado = query.Estado= false,

                };
                dbRecursosHumanosEntities.SaveChanges();
                TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha modificado con exito.", Titulo = "Observaciones" };
                return Json(new { Estatu = true, Mensaje = "Se ha modificado con exito." },
                    JsonRequestBehavior.AllowGet);

            }
        }

        [HttpPost]
        public ActionResult Deletee(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        // POST: Area/Delete/5

    }
}

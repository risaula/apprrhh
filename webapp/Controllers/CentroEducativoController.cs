﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SmartAdminMvc.Database;
using SmartAdminMvc.Models.Areas;
using SmartAdminMvc.Models.Base;
using SmartAdminMvc.Models.CentroEducativo;
using SmartAdminMvc.Models.Idiomas;

namespace SmartAdminMvc.Controllers
{
    [Authorize]

    public class CentroEducativoController : Controller
    {
        // GET: CentroEducativo
        public ActionResult Index()
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {
                 var ListarC = dbRecursosHumanosEntities.CentroEducativoes.Select(x =>
                    new ListarCentroEducativoViewModel
                    {
                       IdCentroEducativo = x.IdCentroEducativo,
                    EstadoCentroEducativo = x.EstadoCentroEducativo,
                    Telefono = x.Telefono,
                    Direccion = x.Direccion,
                      Nombre = x.Nombre
                    }).ToList();
                return View(ListarC);
            }
        }

        // GET: CentroEducativo/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: CentroEducativo/Create
        public ActionResult Create()
        {
            return PartialView(new CrearCentroEducativoViewModel());
        }

        // POST: CentroEducativo/Create
        [HttpPost]
        public JsonResult Create(CrearCentroEducativoViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Estatus = false, Mensaje = "Revisar campos requeridos." },
                        JsonRequestBehavior.AllowGet);
                }

                using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
                {
                    var nuevoC = new CentroEducativo
                    {
                        Nombre = model.Nombre,
                        EstadoCentroEducativo = true,
                        Telefono = model.Telefono,
                        Direccion = model.Direccion,
                        CreadoPor = User.Identity.Name,
                        ModificadoPor = User.Identity.Name,
                        FechaCreacion = DateTime.Now,
                        FechaModificado = DateTime.Now

                    };
                    dbRecursosHumanosEntities.CentroEducativoes.Add(nuevoC);
                    dbRecursosHumanosEntities.SaveChanges();

                    TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha guardado de manera exitosa.", Titulo = "Centro Educativo" };
                    return Json(new { Estatus = true, Mensaje = "Se ha  guardado con exito." },
                        JsonRequestBehavior.AllowGet);


                }
            }
            catch (Exception ex)
            {

                return Json(new { Estatus = false, Mensaje = ex.Message },
                    JsonRequestBehavior.AllowGet);

            }
        }

        // GET: CentroEducativo/Edit/5
        public ActionResult Edit(int id)
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {
                var query = dbRecursosHumanosEntities.CentroEducativoes.FirstOrDefault(x => x.IdCentroEducativo == id);
                var modelo = new EditarCentroEducativoViewModel
                {
                    IdCentroEducativo = query.IdCentroEducativo,
                    Telefono = query.Telefono,
                    Direccion = query.Direccion,
                    Nombre = query.Nombre,

                };
                return PartialView(modelo);
            }
        }

        // POST: CentroEducativo/Edit/5
        [HttpPost]
        public ActionResult Edit(EditarCentroEducativoViewModel modelo)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new {Estatu = false, Mensaje = "Revisar campos requeridos."},
                        JsonRequestBehavior.AllowGet);
                }
                using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
                {
                    var query = dbRecursosHumanosEntities.CentroEducativoes.FirstOrDefault(x =>
                        x.IdCentroEducativo == modelo.IdCentroEducativo);
                    query.Nombre = modelo.Nombre;
                    query.Telefono = modelo.Telefono;
                    query.Direccion = modelo.Direccion;

                    dbRecursosHumanosEntities.SaveChanges();

                    TempData["MensajeDeRespuesta"] = new RespuestaBase
                    {
                        Estatus = true,
                        Mensage = "Se ha modificado con exito.",
                        Titulo = "Centro Educativo"
                    };
                    return Json(new {Estatu = true, Mensaje = "Se ha modificado con exito."},
                        JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new {Estatus = false, Mensaje = ex.Message},
                    JsonRequestBehavior.AllowGet);
            }
        }

        // GET: CentroEducativo/Delete/5
            public ActionResult Delete(int id)
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {

                var query = dbRecursosHumanosEntities.CentroEducativoes.FirstOrDefault(x => x.IdCentroEducativo == id);

                var modelo = new EditarCentroEducativoViewModel
                {


                    EstadoCentroEducativo = query.EstadoCentroEducativo = false,

                };
                dbRecursosHumanosEntities.SaveChanges();
                TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha modificado con exito.", Titulo = "Centro Educativo" };
                return Json(new { Estatu = true, Mensaje = "Se ha modificado con exito." },
                    JsonRequestBehavior.AllowGet);

            }
        }

        // POST: CentroEducativo/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

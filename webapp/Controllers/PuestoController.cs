﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SmartAdminMvc.Models;
using SmartAdminMvc.Database;
using System.Data.Entity.Infrastructure;
using SmartAdminMvc.Models.Areas;
using SmartAdminMvc.Models.Departamentos;
using SmartAdminMvc.Controllers;
using SmartAdminMvc.Models.Base;
using SmartAdminMvc.Models.Puesto;

namespace SmartAdminMvc.Controllers
{
    [Authorize]
    public class PuestoController : Controller
    {
        // GET: Puesto
        public ActionResult Index()
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {
                var ListarPuesto = dbRecursosHumanosEntities.Puestoes.Select(x =>
                    new ListarPuestoViewModel
                    {
                        IdPuesto = x.IdPuesto,
                        NameArea = x.Area.Nombre,
                        Estado = x.Estado,
                        Puesto = x.DescripcionPuesto
                    }).ToList();
                return View(ListarPuesto);
            }
        }

        // GET: Puesto/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Puesto/Create
        public ActionResult Create()
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {

                ViewBag.SelectArea = dbRecursosHumanosEntities.Areas.Where(x=>x.EstadoArea==true).ToList()
                    //var listadoDeAreas = dbRecursosHumanosEntities.Areas.Where(x=>x.EstadoArea==true  ).Select(x => new ListarAreasViewModel

                    .Select(x => new SelectListItem
                    {
                        Text = x.Nombre,
                        Value = x.IdArea.ToString()
                    }).ToList();
                return PartialView(new CrearPuestioViewModel());
            }
        }

        // POST: Puesto/Create
        [HttpPost]
        public JsonResult Create(CrearPuestioViewModel modelo)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new {Estatus = false, Mensaje = "Revisar campos requeridos."},
                        JsonRequestBehavior.AllowGet);
                }

                using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
                {
                    var nuevaPuesto = new Puesto
                    {
                        IdArea = modelo.IdArea,
                        DescripcionPuesto = modelo.Puesto,
                        CreadoPor = User.Identity.Name,
                        ModificadoPor = User.Identity.Name,
                        Estado = true,
                        FechaCreacion = DateTime.Now,
                        FechaModificado = DateTime.Now
                    };
                    var condiPro = dbRecursosHumanosEntities.Puestoes.Any(x =>
                        x.DescripcionPuesto == modelo.Puesto && x.IdArea == modelo.IdArea);
                    if (condiPro == true)
                    {
                        return Json(new {Estatus = false, Mensaje = "Nombre de Puestos ya Existente."},
                            JsonRequestBehavior.AllowGet);
                    }
                    dbRecursosHumanosEntities.Puestoes.Add(nuevaPuesto);

                    dbRecursosHumanosEntities.SaveChanges();

                    TempData["MensajeDeRespuesta"] = new RespuestaBase
                    {
                        Estatus = true,
                        Mensage = "Se ha guardado de manera exitosa.",
                        Titulo = "Puestos"
                    };
                    return Json(new {Estatus = true, Mensaje = "Se ha  guardado con exito."},
                        JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {

                return Json(new {Estatus = false, Mensaje = ex.Message},
                    JsonRequestBehavior.AllowGet);

            }
        }

        // GET: Puesto/Edit/5
        public ActionResult Edit(int id)
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {
                ViewBag.SelectArea = dbRecursosHumanosEntities.Areas.ToList()
                    .Select(x => new SelectListItem
                    {
                        Text = x.Nombre,
                        Value = x.IdArea.ToString()
                    }).ToList();
                var query = dbRecursosHumanosEntities.Puestoes.FirstOrDefault(x => x.IdPuesto == id);
                var modelo = new EditarPuestoViewModel()
                {

                    IdArea = (int) query.IdArea,

                    IdPuesto = query.IdPuesto,

                    Puesto = query.DescripcionPuesto,

                };

                return PartialView(modelo);
            }
        }
    

// POST: Puesto/Edit/5
        [HttpPost]
     public ActionResult Edit(EditarPuestoViewModel modelo)
            {
                try
                {
                    if (!ModelState.IsValid)
                    {
                    return Json(new { Estatu = false, Mensaje = "Revisar campos requeridos." },
                        JsonRequestBehavior.AllowGet);
                }
                    using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
                {
                    var query = dbRecursosHumanosEntities.Puestoes.FirstOrDefault(x => x.IdPuesto == modelo.IdPuesto);
                    var condiPro = dbRecursosHumanosEntities.Puestoes.Any(x => x.DescripcionPuesto == modelo.Puesto && x.IdArea == modelo.IdArea);

                    if (condiPro == true)
                    {
                        return Json(new { Estatus = false, Mensaje = "Nombre de Puesto ya Existente." },
                            JsonRequestBehavior.AllowGet);
                    }
                    query.IdArea = modelo.IdArea;

                    query.DescripcionPuesto = modelo.Puesto;

                    dbRecursosHumanosEntities.SaveChanges();

                    TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha modificado con exito.", Titulo = "Puestos" };
                    return Json(new { Estatu = true, Mensaje = "Se ha modificado con exito." },
                        JsonRequestBehavior.AllowGet);
                }
                }
            catch (Exception ex)
            {
                return Json(new { Estatus = false, Mensaje = ex.Message },
                    JsonRequestBehavior.AllowGet);
            }
        }

        // GET: Puesto/Delete/5
        public ActionResult Delete(int id)
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {

                var query = dbRecursosHumanosEntities.Puestoes.FirstOrDefault(x => x.IdPuesto == id);

                var modelo = new EditarPuestoViewModel
                {


                    Estado = query.Estado = false,

                };
                dbRecursosHumanosEntities.SaveChanges();
                TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha modificado con exito.", Titulo = "Puesto" };
                return Json(new { Estatu = true, Mensaje = "Se ha modificado con exito." },
                    JsonRequestBehavior.AllowGet);

            }
        }

        // POST: Puesto/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

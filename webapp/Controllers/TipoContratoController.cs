﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SmartAdminMvc.Models.Base;
using SmartAdminMvc.Database;
using SmartAdminMvc.Models.TipoContrato;

namespace SmartAdminMvc.Controllers
{
    [Authorize]
    public class TipoContratoController : Controller
    {
        // GET: TipoContrato
        public ActionResult Index()
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {
                var listaContrato = dbRecursosHumanosEntities.TipoContratoes.Select(x => new TipoContratoViewModel() { IdContrato = x.IdTipoContrato, Nombre = x.Descripcion, Estado = x.EstadoTipoContrato }).ToList();
                return View(listaContrato);
            }
        }

        // GET: TipoContrato/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: TipoContrato/Create
        public ActionResult Create()
        {
            return PartialView(new CraerTipoContratoViewModel());
        }

        // POST: TipoContrato/Create
        [HttpPost]
        public JsonResult Create(CraerTipoContratoViewModel modelo)
        {

            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Estatus = false, Mensaje = "Revisar campos requeridos." },
                        JsonRequestBehavior.AllowGet);
                }
                using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
                { 
                    var nuevoContrato = new TipoContrato
                {
                    Descripcion = modelo.Nombre,
                    EstadoTipoContrato = true,
                    CreadoPor = User.Identity.Name,
                    ModificadoPor = User.Identity.Name,
                    FechaCreacion = DateTime.Now,
                    FechaModificado = DateTime.Now

                };
                dbRecursosHumanosEntities.TipoContratoes.Add(nuevoContrato);
                dbRecursosHumanosEntities.SaveChanges();

                TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha guardado de manera exitosa.", Titulo = "Contrato" };
                return Json(new { Estatus = true, Mensaje = "Se ha  guardado con exito." },
                    JsonRequestBehavior.AllowGet);


            }
            }
            catch (Exception ex)
            {

                return Json(new { Estatus = false, Mensaje = ex.Message },
                    JsonRequestBehavior.AllowGet);

            }
        }


        // GET: TipoContrato/Edit/5
        public ActionResult Edit(int id)
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {
                
                var query = dbRecursosHumanosEntities.TipoContratoes.FirstOrDefault(x => x.IdTipoContrato == id);
                var modelo = new EditarTipoContratoViewModel()
                {
                    IdContrato = query.IdTipoContrato,
                    Nombre = query.Descripcion,

                };
                return PartialView(modelo);
            }
        }

        // POST: TipoContrato/Edit/5
        [HttpPost]
        public ActionResult Edit(EditarTipoContratoViewModel modelo)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Estatu = false, Mensaje = "Revisar campos requeridos." },
                        JsonRequestBehavior.AllowGet);
                }
                using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
                {

                    var query = dbRecursosHumanosEntities.TipoContratoes.FirstOrDefault(x => x.IdTipoContrato == modelo.IdContrato);
                    query.Descripcion = modelo.Nombre;

                    dbRecursosHumanosEntities.SaveChanges();

                    TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha modificado con exito.", Titulo = "Contratos" };
                    return Json(new { Estatu = true, Mensaje = "Se ha modificado con exito." },
                        JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Estatus = false, Mensaje = ex.Message },
                    JsonRequestBehavior.AllowGet);
            }
        }

        // GET: TipoContrato/Delete/5
        public ActionResult Delete(int id)
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {

                var query = dbRecursosHumanosEntities.TipoContratoes.FirstOrDefault(x => x.IdTipoContrato == id);

                var modelo = new EditarTipoContratoViewModel
                {


                    Estado = query.EstadoTipoContrato = false,

                };
                dbRecursosHumanosEntities.SaveChanges();
                TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha modificado con exito.", Titulo = "Tipo Contrato" };
                return Json(new { Estatu = true, Mensaje = "Se ha modificado con exito." },
                    JsonRequestBehavior.AllowGet);

            }
        }

        // POST: TipoContrato/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

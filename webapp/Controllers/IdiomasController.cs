﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;
using SmartAdminMvc.Database;
using SmartAdminMvc.Models.Base;
using SmartAdminMvc.Models.Idiomas;
using SmartAdminMvc.Models.Puesto;

namespace SmartAdminMvc.Controllers
{
    [Authorize]
    public class IdiomasController : Controller
    {
        // GET: Idiomas
        public ActionResult Index()
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {
                var ListarIdiomas = dbRecursosHumanosEntities.Idiomas.Select(x => new ListarIdiomasViewModel { IdIdioma = x.IdIdiomas, NombreIdioma = x.Descripcion , EstadoIdioma = x.EstadoIdioma}).ToList();
                return View(ListarIdiomas);
            }
        }

        // GET: Idiomas/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Idiomas/Create
        public ActionResult Create()
        {

            return PartialView(new CrearIdiomasViewModel());

        }

        // POST: Idiomas/Create
        [HttpPost]
        public JsonResult Create(CrearIdiomasViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Estatus = false, Mensaje = "Revisar campos requeridos." },
                        JsonRequestBehavior.AllowGet);
                }

                using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
                {
                    var nuevoidioma = new Idioma
                    {
                        Descripcion = model.NombreIdioma,
                        EstadoIdioma = true,
                        CreadoPor = User.Identity.Name,
                        ModificadoPor = User.Identity.Name,
                        FechaCreacion = DateTime.Now,
                        FechaModificado = DateTime.Now

                    };
                    dbRecursosHumanosEntities.Idiomas.Add(nuevoidioma);
                    dbRecursosHumanosEntities.SaveChanges();

                    TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha guardado de manera exitosa.", Titulo = "Idioma" };
                    return Json(new { Estatus = true, Mensaje = "Se ha  guardado con exito." },
                        JsonRequestBehavior.AllowGet);


                }
            }
            catch (Exception ex)
            {

                return Json(new { Estatus = false, Mensaje = ex.Message },
                    JsonRequestBehavior.AllowGet);

            }
        }


        // GET: Idiomas/Edit/5
        public ActionResult Edit(int id)
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {
                var query = dbRecursosHumanosEntities.Idiomas.FirstOrDefault(x => x.IdIdiomas == id);
                var modelo = new EditarIdiomasViewModel()
                {
                    IdIdioma = query.IdIdiomas,
                   NombreIdioma = query.Descripcion,

                };
                return PartialView(modelo);
            }
        }

        // POST: Idiomas/Edit/5
        [HttpPost]
        public ActionResult Edit(EditarIdiomasViewModel modelo)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Estatu = false, Mensaje = "Revisar campos requeridos." },
                        JsonRequestBehavior.AllowGet);
                }
                using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
                {
                    var query = dbRecursosHumanosEntities.Idiomas.FirstOrDefault(x => x.IdIdiomas == modelo.IdIdioma);
                    query.Descripcion = modelo.NombreIdioma;

                    dbRecursosHumanosEntities.SaveChanges();

                    TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha modificado con exito.", Titulo = "Idiomas" };
                    return Json(new { Estatu = true, Mensaje = "Se ha modificado con exito." },
                        JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Estatus = false, Mensaje = ex.Message },
                    JsonRequestBehavior.AllowGet);
            }
        }

        // GET: Idiomas/Delete/5
        public ActionResult Delete(int id)
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {

                var query = dbRecursosHumanosEntities.Idiomas.FirstOrDefault(x => x.IdIdiomas == id);

                var modelo = new EditarIdiomasViewModel
                {


                    EstadoIdioma = query.EstadoIdioma = false,

                };
                dbRecursosHumanosEntities.SaveChanges();
                TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha modificado con exito.", Titulo = "Idiomas" };
                return Json(new { Estatu = true, Mensaje = "Se ha modificado con exito." },
                    JsonRequestBehavior.AllowGet);

            }
        }

        // POST: Idiomas/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

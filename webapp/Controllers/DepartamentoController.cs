﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SmartAdminMvc.Database;
using SmartAdminMvc.Models.Base;
using SmartAdminMvc.Models.Departamentos;
using SmartAdminMvc.Models.Idiomas;

namespace SmartAdminMvc.Controllers
{
    [Authorize]
    public class DepartamentoController : Controller
    {
        // GET: Departamento
        public ActionResult Index()
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {
                var ListarDep = dbRecursosHumanosEntities.Departamentoes.Select(x => new
                ListarDepartamentoViewModel
                    {
                        IdDepartamento = x.IdDepartamento,
                    NombreDepartamento = x.NombreDepartamento,
                    EstadoDepartamento = x.EstadoDepartamento
                    }).ToList();
                return View(ListarDep);
            }
        }

        // GET: Departamento/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Departamento/Create
        public ActionResult Create()
        {
            return PartialView(new CrearDepartamentoViewModel());
        }

        // POST: Departamento/Create
        [HttpPost]
        public JsonResult Create(CrearDepartamentoViewModel modelo)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Estatus = false, Mensaje = "Revisar campos requeridos." },
                        JsonRequestBehavior.AllowGet);
                }

                using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
                {
                    var nuevoidioma = new Departamento
                    {
                        NombreDepartamento = modelo.NombreDepartamento,
                        EstadoDepartamento = "true",
                        CreadoPor = User.Identity.Name,
                        ModificadoPor = User.Identity.Name,
                        FechaCreacion = DateTime.Now,
                        FechaModificado = DateTime.Now

                    };
                    dbRecursosHumanosEntities.Departamentoes.Add(nuevoidioma);
                    dbRecursosHumanosEntities.SaveChanges();

                    TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha guardado de manera exitosa.", Titulo = "Departamento" };
                    return Json(new { Estatus = true, Mensaje = "Se ha  guardado con exito." },
                        JsonRequestBehavior.AllowGet);


                }
            }
            catch (Exception ex)
            {

                return Json(new { Estatus = false, Mensaje = ex.Message },
                    JsonRequestBehavior.AllowGet);

            }
        }

        // GET: Departamento/Edit/5
        public ActionResult Edit(int id)
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {
                var query = dbRecursosHumanosEntities.Departamentoes.FirstOrDefault(x => x.IdDepartamento == id);
                var modelo = new EditarDepartamentoViewModel()
                {
                    IdDepartamento = query.IdDepartamento,
                    NombreDepartamento= query.NombreDepartamento,

                };
                return PartialView(modelo);
            }
        }

        // POST: Departamento/Edit/5
        [HttpPost]
        public ActionResult Edit(EditarDepartamentoViewModel modelo)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Estatu = false, Mensaje = "Revisar campos requeridos." },
                        JsonRequestBehavior.AllowGet);
                }
                using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
                {
                    var query = dbRecursosHumanosEntities.Departamentoes.FirstOrDefault(x => x.IdDepartamento== modelo.IdDepartamento);
                    query.NombreDepartamento = modelo.NombreDepartamento;

                    dbRecursosHumanosEntities.SaveChanges();

                    TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha modificado con exito.", Titulo = "Idiomas" };
                    return Json(new { Estatu = true, Mensaje = "Se ha modificado con exito." },
                        JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Estatus = false, Mensaje = ex.Message },
                    JsonRequestBehavior.AllowGet);
            }
        }

        // GET: Departamento/Delete/5
        public ActionResult Delete(int id)
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {

                var query = dbRecursosHumanosEntities.Departamentoes.FirstOrDefault(x => x.IdDepartamento == id);

                var modelo = new EditarDepartamentoViewModel
                {


                    EstadoDepartamento = query.EstadoDepartamento = "false",

                };
                dbRecursosHumanosEntities.SaveChanges();
                TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha modificado con exito.", Titulo = "Departamento" };
                return Json(new { Estatu = true, Mensaje = "Se ha modificado con exito." },
                    JsonRequestBehavior.AllowGet);

            }
        }

        // POST: Departamento/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

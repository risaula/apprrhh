﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SmartAdminMvc.Models;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using SmartAdminMvc.Database;
using SmartAdminMvc.Models.Areas;
using SmartAdminMvc.Controllers;
using SmartAdminMvc.Models.Base;


namespace SmartAdminMvc.Controllers
{
    [Authorize]
    public class AreaController : Controller
    {

        // GET: Area
      
        public ActionResult Index()
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {
                   var ListarAreas = dbRecursosHumanosEntities.Areas.Select(x =>
                    new ListarAreasViewModel
                    {
                   IdArea = x.IdArea,
                    Departamento = x.Departamento.NombreDepartamento,
                    Estado = x.EstadoArea,
                    NombreArea = x.Nombre
                }).ToList();
                return View(ListarAreas);
            }
        }


        // GET: Area/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Area/Create
        public ActionResult Create()
        {

            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {

                ViewBag.SelectDepartamentos = dbRecursosHumanosEntities.Departamentoes.Where(x=>x.EstadoDepartamento=="true").ToList()
                    .Select(x => new SelectListItem
                    {
                        Text = x.NombreDepartamento,
                        Value = x.IdDepartamento.ToString()
                    }).ToList();

                return PartialView(new CrearAreaViewModel());

            }

        }

        // POST: Persona/Create
        [HttpPost]
        public JsonResult Create(CrearAreaViewModel modelo)
        {

            try
            {

                if (!ModelState.IsValid)
                {
                    return Json(new { Estatus = false, Mensaje = "Revisar campos requeridos." },
                        JsonRequestBehavior.AllowGet);
                }

                using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
                {

                    var nuevaArea = new Area
                    {
                        IdDepartamento = modelo.IdDepartamento,
                        Nombre = modelo.NombreArea,
                        CreadoPor = User.Identity.Name,
                        ModificadoPor = User.Identity.Name,
                        EstadoArea = true,
                        FechaCreacion = DateTime.Now,
                        FechaModificado = DateTime.Now
                    };
                    var condiArea = dbRecursosHumanosEntities.Areas.Any(x => x.Nombre == modelo.NombreArea && x.IdDepartamento == modelo.IdDepartamento);

                    if (condiArea == true)
                    {
                        return Json(new { Estatus = false, Mensaje = "Nombre de Area ya Existente." },
                            JsonRequestBehavior.AllowGet);
                    }
                    dbRecursosHumanosEntities.Areas.Add(nuevaArea);

                    dbRecursosHumanosEntities.SaveChanges();

                    TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha guardado de manera exitosa.", Titulo = "Areas" };
                    return Json(new { Estatus = true, Mensaje = "Se ha  guardado con exito." },
                        JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {

                return Json(new { Estatus = false, Mensaje = ex.Message },
                    JsonRequestBehavior.AllowGet);

            }

        }


        // GET: Area/Edit/5
        public ActionResult Edit(int id)
        {

            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {

                ViewBag.SelectDepartamentos = dbRecursosHumanosEntities.Departamentoes.ToList()
                    .Select(x => new SelectListItem
                    {
                        Text = x.NombreDepartamento,
                        Value = x.IdDepartamento.ToString()
                    }).ToList();

                var query = dbRecursosHumanosEntities.Areas.FirstOrDefault(x => x.IdArea == id);

                var modelo = new EditarAreaViewModel
                {

                    IdDepartamento = (int)query.IdDepartamento,

                    IdArea = query.IdArea,

                    NombreArea = query.Nombre,

                };

                return PartialView(modelo);

            }

        }

        // POST: Persona/Edit/5
        [HttpPost]
        public ActionResult Edit(EditarAreaViewModel modelo)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Estatu = false, Mensaje = "Revisar campos requeridos." },
                        JsonRequestBehavior.AllowGet);
                }
                using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
                {

                    var query = dbRecursosHumanosEntities.Areas.FirstOrDefault(x => x.IdArea == modelo.IdArea);
                    var condiArea = dbRecursosHumanosEntities.Areas.Any(x => x.Nombre == modelo.NombreArea && x.IdDepartamento == modelo.IdDepartamento);

                    if (condiArea == true)
                    {
                        return Json(new { Estatus = false, Mensaje = "Nombre de Area ya Existente." },
                            JsonRequestBehavior.AllowGet);
                    }
                    query.IdDepartamento = modelo.IdDepartamento;

                    query.Nombre = modelo.NombreArea;

                    dbRecursosHumanosEntities.SaveChanges();

                    TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha modificado con exito.", Titulo = "Areas" };
                    return Json(new { Estatu = true, Mensaje = "Se ha modificado con exito." },
                        JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                return Json(new { Estatus = false, Mensaje = ex.Message },
                    JsonRequestBehavior.AllowGet);
            }
        }
        // GET: Area/Delete/5
        public ActionResult Delete(int id)
        {
            using (var dbRecursosHumanosEntities = new RecursosHumanosEntitie())
            {

           var query = dbRecursosHumanosEntities.Areas.FirstOrDefault(x => x.IdArea == id);

                var modelo = new EditarAreaViewModel
                {


                    Estado = query.EstadoArea=false,

                };
                dbRecursosHumanosEntities.SaveChanges();
                TempData["MensajeDeRespuesta"] = new RespuestaBase { Estatus = true, Mensage = "Se ha modificado con exito.", Titulo = "Areas" };
                return Json(new { Estatu = true, Mensaje = "Se ha modificado con exito." },
                    JsonRequestBehavior.AllowGet);

            }
        }

        [HttpPost]
        public ActionResult Deletee(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        // POST: Area/Delete/5
        
    }
}

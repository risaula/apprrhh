@model SmartAdminMvc.Models.Personas.CrearCandidatoViewModel

<style>
    /* Let's get this party started */
    ::-webkit-scrollbar {
        width: 5px;
    }

    /* Track */
    ::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 6px rgba(28, 128, 128,0.3);
        -webkit-border-radius: 5px;
        border-radius: 4px;
    }

    /* Handle */
    ::-webkit-scrollbar-thumb {
        -webkit-border-radius: 5px;
        border-radius: 4px;
        background: rgba(128, 128, 128,0.8);
        -webkit-box-shadow: inset 0 0 6px rgba(128, 128, 128,0.3);
    }

        ::-webkit-scrollbar-thumb:window-inactive {
            background: rgba(28, 128, 128,0.4);
        }

    .smart-form * {
        box-sizing: border-box !important;
    }
</style>

<div class="row">
    <div class="col-lg-6 col-lg-offset-3">

        <div class="jarviswidget jarviswidget-color-darken">
            <header role="heading">
                <h2>
                    <span class="fa fa-edit">

                    </span>
                    Contratado
                </h2>
            </header>
            <div>
                <div class="widget-body no-padding">
                    <fieldset>
                        <div class="row">
                            <div class="col-lg-4 text-center">
                                <div class="smart-form">
                                    <fieldset>
                                        <legend>  Logo</legend>
                                        @*<img id="preViewImg" src="@Model.UrlImagen" class="img-responsive img-thumbnail img-rounded" style="width: auto; height:213px;" />*@
                                    </fieldset>
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <div class="smart-form">
                                    @Html.AntiForgeryToken()
                                    @Html.ValidationSummary(true, "", new { @class = "text-danger" })
                                    <fieldset>
                                        <legend>Crear Contratado</legend>
                                        <section class="col col-6">
                                            <div class="form-group">
                                                @Html.LabelFor(model => model.Nombres, htmlAttributes: new { @class = "control-label" })
                                                  <label class="input">
                                                    @Html.EditorFor(model => model.Nombres, new {htmlAttributes = new {@class = "form-control"}})
                                                    @Html.HiddenFor(model => model.IdPersona)
                                                    @Html.ValidationMessageFor(model => model.Nombres, "", new {@class = "text-danger"})
                                                </label>
                                            </div>
                                        </section>
                                        <section class="col col-6">
                                            <div class="form-group">
                                                @Html.LabelFor(model => model.Apellido, htmlAttributes: new { @class = "control-label" })
                                                <label class="input">
                                                    @Html.EditorFor(model => model.Apellido, new { htmlAttributes = new { @class = "form-control" } })
                                                    @Html.ValidationMessageFor(model => model.Apellido, "", new { @class = "text-danger" })
                                                </label>
                                            </div>
                                        </section>
                                        <section class="col col-6">
                                            <div class="form-group">
                                                @Html.LabelFor(model => model.Nacionalidads, htmlAttributes: new {@class = "control-label"})
                                                <label class="input">
                                                    @Html.DropDownListFor(model => model.Nacionalidads, new SelectList(ViewBag.SelectNacionalidad, "Value", "Text"), "", new { @class = "form-control", @style = "width:100%;" })
                                                    @Html.ValidationMessageFor(model => model.Nacionalidads, "", new { @class = "text-danger" })
                                                </label>
                                            </div>
                                        </section>
                                        <section class="col col-6">
                                            <div class="form-group">
                                                @Html.LabelFor(model => model.NumeroIdentidads, htmlAttributes: new {@class = "control-label"})
                                                <label class="input">
                                                    @Html.EditorFor(model => model.NumeroIdentidads, new { htmlAttributes = new { @class = "form-control" } })
                                                    @Html.ValidationMessageFor(model => model.NumeroIdentidads, "", new { @class = "text-danger" })
                                                </label>
                                            </div>
                                        </section>
                                        <section class="col col-6">
                                            <div class="form-group">
                                                @Html.LabelFor(model => model.Generos, htmlAttributes: new { @class = "control-label" })
                                                <label class="input">
                                                    @Html.DropDownListFor(model => model.Generos, new SelectList(ViewBag.SelectGenero, "Value", "Text"), "", new { @class = "form-control", @style = "width:100%;" })
                                                    @Html.ValidationMessageFor(model => model.Generos, "", new { @class = "text-danger" })
                                                </label>
                                            </div>
                                        </section>
                                        <section class="col col-6">
                                            <div class="form-group">
                                                @Html.LabelFor(model => model.FechaNacimientos, htmlAttributes: new { @class = "control-label" })
                                                <label class="input">
                                                    @Html.EditorFor(model => model.FechaNacimientos, new { htmlAttributes = new { @class = "form-control" } })
                                                    @Html.ValidationMessageFor(model => model.FechaNacimientos, "", new { @class = "text-danger" })
                                                </label>
                                            </div>
                                        </section>
                                    </fieldset>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="smart-form">
                                    <form class="smart-form" id="frmPersonas" method="post" enctype="multipart/form-data">
                                        <fieldset>
                                            <legend>Otros Datos Personales</legend>
                                            <section class="col col-6">
                                                <div class="form-group">
                                                    @*@Html.LabelFor(model => model.IdNacionalidad, htmlAttributes: new { @class = "control-label" })*@
                                                    @Html.LabelFor(model => model.Puestos, htmlAttributes: new { @class = "control-label" })
                                                    <label class="input">
                                                        @Html.DropDownListFor(model => model.Puestos, new SelectList(ViewBag.SelectPuesto, "Value", "Text"), "", new { @class = "form-control select2 ValidateRequired" })
                                                        @Html.ValidationMessageFor(model => model.Puestos, "", new { @class = "text-danger" })
                                                    </label>
                                                </div>
                                            </section>
                                            <section class="col col-3">
                                                <div class="form-group">
                                                    @Html.LabelFor(model => model.TipoContra, htmlAttributes: new { @class = "control-label" })
                                                    @*@Html.DisplayTranslatedMessage(3613)*@
                                                    <label class="input">
                                                        @Html.DropDownListFor(model => model.TipoContra, new SelectList(ViewBag.SelectContrato, "Value", "Text"), "", new { @class = "form-control select2 ValidateRequired" })
                                                        @Html.ValidationMessageFor(model => model.TipoContra, "", new { @class = "text-danger" })
                                                    </label>
                                                </div>
                                            </section>
                                            <section class="col col-3">
                                                <div class="form-group">
                                                    @Html.LabelFor(model => model.TipoPlan, htmlAttributes: new { @class = "control-label" })
                                                    @*@Html.DisplayTranslatedMessage(3672)*@
                                                    <label class="input">
                                                        @Html.DropDownListFor(model => model.TipoPlan, new SelectList(ViewBag.SelectPlanilla, "Value", "Text"), "", new { @class = "form-control select2 ValidateRequired" })
                                                        @Html.ValidationMessageFor(model => model.TipoPlan, "", new { @class = "text-danger" })
                                                    </label>
                                                </div>
                                            </section>
                                            <section class="col col-6">
                                                <div class="form-group">
                                                    @Html.LabelFor(model => model.TipoTurno, htmlAttributes: new { @class = "control-label" })
                                                    @*@Html.DisplayTranslatedMessage(3616)*@
                                                    <label class="input">
                                                        @Html.DropDownListFor(model => model.TipoTurno, new SelectList(ViewBag.SelectTurno, "Value", "Text"), "", new { @class = "form-control select2 ValidateRequired" })
                                                        @Html.ValidationMessageFor(model => model.TipoTurno, "", new { @class = "text-danger" })
                                                    </label>
                                                </div>
                                            </section>
                                            <section class="col col-6">
                                                <div class="form-group">
                                                    @Html.LabelFor(model => model.FechaInicio, htmlAttributes: new { @class = "control-label" })
                                                 <label class="input">
                                                     @Html.EditorFor(model => model.FechaInicio, new { htmlAttributes = new { @class = "form-control" } })
                                                        @Html.ValidationMessageFor(model => model.FechaInicio, "", new { @class = "text-danger" })
                                                    </label>
                                                </div>
                                            </section>

                                            <section class="col col-6">
                                                <div class="form-group">
                                                    @Html.LabelFor(model => model.SueldoBase, htmlAttributes: new { @class = "control-label" })
                                                    @*@Html.DisplayTranslatedMessage(3584)*@
                                                    <label class="input">
                                                        <i class="icon-prepend">L.</i>
                                                        @Html.EditorFor(model => model.SueldoBase, new { htmlAttributes = new { @class = "form-control input-lg", required = "required" } })
                                                        @Html.ValidationMessageFor(model => model.SueldoBase, "", new { @class = "text-danger" })
                                                    </label>
                                                </div>
                                            </section>
                                            <section class="col col-6">
                                                <div class="form-group">
                                                    @Html.LabelFor(model => model.CodigoEmpleado, htmlAttributes: new { @class = "control-label" })
                                                    @*@Html.DisplayTranslatedMessage(3674)*@
                                                    <label class="input">
                                                        <i class="icon-prepend fa fa-user"></i>
                                                        @Html.EditorFor(model => model.CodigoEmpleado, new { htmlAttributes = new { @class = "form-control", required = "required" } })
                                                        @Html.ValidationMessageFor(model => model.CodigoEmpleado, "", new { @class = "text-danger" })
                                                    </label>
                                                </div>
                                            </section>
                                            <section class="col col-6">
                                                <br />
                                                <div class="form-group">
                                                    @Html.LabelFor(model => model.EstadoContrato, htmlAttributes: new { @class = "control-label col-md-2" })
                                                    <div class="col-md-10">
                                                        @Html.EditorFor(model => model.EstadoContrato, new { htmlAttributes = new { @class = "form-control" } })
                                                        @Html.ValidationMessageFor(model => model.EstadoContrato, "", new { @class = "text-danger" })
                                                    </div>
                                                </div>
                                            </section>
                                        </fieldset>
                                        <div class="form-group">
                                            <div class="col-md-offset-2 col-md-10">
                                                <input type="submit" value="Create" class="btn btn-default" />
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>

        </div>
    </div>
</div>


<div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false">
    <div class="modal-dialog" role="document" id="BodyModal">

    </div>
</div>




<div>
    @Html.ActionLink("Back to List", "Index")
</div>
@section Scripts {
    @Scripts.Render("~/bundles/jqueryval")

    <script>
        $(document).ready(function () {
            @*//$("#FechaNacimiento").val("@Model.FechaNacimiento.ToString("dd/MM/yyyy")");
            $("#FechaNacimiento").datepicker({
                startDate: new Date,
                dateFormat: 'dd/MM/yy',
                changeMonth: true,
                changeYear: true,
                prevText: '<i class="fa fa-chevron-left"></i>',
                nextText: '<i class="fa fa-chevron-right"></i>'
            });*@
            $("#FechaEgresoAntecedentes").datepicker({
                startDate: new Date,
                dateFormat: 'yy/MM/dd',
                changeMonth: true,
                changeYear: true,
                prevText: '<i class="fa fa-chevron-left"></i>',
                nextText: '<i class="fa fa-chevron-right"></i>'
            });
            $("#FechaIngresoAntecedentes").datepicker({
                startDate: new Date,
                dateFormat: 'yy/MM/dd',
                changeMonth: true,
                changeYear: true,
                prevText: '<i class="fa fa-chevron-left"></i>',
                nextText: '<i class="fa fa-chevron-right"></i>'
            });
        });
        let flagContratado = false;
        $("select").select2();

        $("#NumeroIdentidads").mask("9999-9999-99999");
        $("#TelefonoMovils").mask("9999-9999");
        $("#TelefonoPersonas").mask("9999-9999");
        $("#TelefonoReferencia").mask("9999-9999");
        $("#TelefonoFijos").mask("9999-9999");
        $("#TelefonoAntecedentesLaborales").mask("9999-9999");
        $("#TelefonoFamiliar").mask("9999-9999");
        $("#FechaNacimientos").datepicker({
            dateFormat: 'dd/MM/yy',
            changeMonth: true,
            changeYear: true,
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>',
            yearRange: '1950:2018'
        });
        $("#FechaInicio").datepicker({
            dateFormat: 'dd/MM/yy',
            changeMonth: true,
            changeYear: true,
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>',
            yearRange: '1950:2018'
        });

    </script>



}


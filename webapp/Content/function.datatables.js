﻿
//(function ($) {

//    $.fn.MiddlewareApplyDataTableWithParams = function (action, controller, params, columns, initComplete, drawCallback, useScroll, pageLength) {
//        var tabla = this;

//        var responsiveHelper_dt_basic = undefined;

//        var breakpointDefinition = {
//            tablet: 1024,
//            phone: 480,
//            desktop: 1366,
//            miniDesktop: 1600
//        };

//        var urlAction = ("/_controller_/_action_").replace("_controller_", controller).replace("_action_", action);
//        var ajaxSettings = {
//            url: urlAction,
//            type: "GET",
//            data: {},
//            dataSrc: function (data) {
//                return data;
//            }
//        };
//        if (params != null) ajaxSettings.data = params;

//        var dataTableSettings = {
//            destroy: true,
//            "sDom": "<'dt-toolbar'<'col-xs-6 col-sm-6'f><'col-sm-6 col-xs-6'l>r>" + "t" +
//            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
//            "oLanguage": {
//                "sSearch": '<div class="icon-addon addon-md"><label for="search" class="glyphicon glyphicon-search" rel="tooltip" title="Search"></label>'
//            },
//            "autoWidth": true,
//            "preDrawCallback": function () {
//                // Initialize the responsive datatables helper once.
//                if (!responsiveHelper_dt_basic) {
//                    responsiveHelper_dt_basic = new ResponsiveDatatablesHelper(tabla, breakpointDefinition);
//                }
//            },
//            "rowCallback": function (nRow) {
//                responsiveHelper_dt_basic.createExpandIcon(nRow);
//            },
//            "drawCallback": function (oSettings) {
//                responsiveHelper_dt_basic.respond();
//            },
//            "pageLength": 10,
//            "ordering": false
//            //"order": [[0, "desc"]]
//        };

//        if (initComplete != null) {
//            dataTableSettings["initComplete"] = initComplete;
//        }


//        if (useScroll != null) {
//            if (useScroll) {
//                dataTableSettings["scrollX"] = true;
//            }
//        }

//        if (drawCallback != null) {
//            dataTableSettings["drawCallback"] = drawCallback;
//        }

//        if (action != null || controller != null) {
//            if (action != "" || controller != "") {
//                dataTableSettings["ajax"] = ajaxSettings;
//            }
//        }

//        if (columns != null) {
//            if (columns.Any()) {
//                dataTableSettings["columns"] = columns;
//            }
//        }

//        if (pageLength != null) {
//            dataTableSettings["pageLength"] = pageLength;

//        }


//        $(this).dataTable(dataTableSettings);
//    }

//    $.fn.MiddlewareReloadDataTableByUrl = function (urlAction) {
//        var idTable = "#" + $(this).attr("id");
//        new $.fn.dataTable.Api(idTable).ajax.url(urlAction).load();
//    }

//    $.fn.MiddlewareReloadDataTable = function () {
//        var idTable = "#" + $(this).attr("id");
//        new $.fn.dataTable.Api(idTable).ajax.reload();
//    }

//})(jQuery);
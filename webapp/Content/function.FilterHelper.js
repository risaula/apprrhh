﻿


(function ($) {

    var origenElement = null;

    var principalContent = $("<div></div>");

    var firstPart = "FilterHelper-";

    var defaults = {

        entityName: null,

        columnName: null,

        size: undefined,

        element: null,

        title: "Helper Filter",

        clear: false

    };

    $.fn.FilterHelper = function (options) {

        origenElement = this;

        defaults.element = this.attr("id");

        defaults = $.extend(defaults, options);

        principalContent.attr("id", (firstPart + defaults.element));

        principalContent.attr("data-helper", true);

        CreateModal();

        var columns = GetColumnXEntity();//.ToColumnDataTable()

        $("#" + "datatable-" + firstPart + defaults.element).MiddlewareApplyDataTableWithParams("GetJsonDataForEntity", "Filter", { entity: defaults.entityName }, columns.ToColumnDataTable());

        $(this).on("dblclick",
            function () {


                $("#dataModal-" + firstPart + defaults.element).modal().show();

            });

        $("#" + "datatable-" + firstPart + defaults.element + " tbody").on("dblclick",
            "tr",
            function () {

                var tabla = new $.fn.dataTable.Api("#" + "datatable-" + firstPart + defaults.element);

                var row = $(this).closest("tr");

                var data = tabla.row(row).data();

               var applyValue = data[defaults.columnName];

                $(origenElement).val(applyValue).trigger("change");

                $("#dataModal-" + firstPart + defaults.element).modal("hide");

            });

    }


    function CreateModal() {

        var modalContent = $('<div class="modal fade" tabindex="-1" role="dialog"></div>');

        modalContent.attr("id", ("dataModal-" + firstPart + defaults.element));

        var modalDialog = $('<div class="modal-dialog" role="document"></div>');

        if (defaults.size !== undefined) {

            modalDialog.addClass("modal-" + defaults.size);

        }
        
        modalContent.append(modalDialog);

        modalDialog.append(CreateJarvisWidget());
        
        //var buttonsFooter = $('<button type="button" class="btn btn-default btn-xs" data-dismiss="modal"><span class="fa fa-remove"></span></button>');

        //var section = $(origenElement).closest("section");

        if (defaults.clear) {
            

            var divs = $("div[data-helper=true]");


            $.each(divs,
                function (idx, dt) {

                    var id = $(dt).attr("id");

                    $("#" + id).html("");


                    $("#" + id).remove();


                });
        }

        principalContent.append(modalContent);

        $("body").append(principalContent);

    }





    function CreateJarvisWidget() {

        var auxiliarDiv = $("<div></div>");

        var jarvisWidget = $("<div></div>");

        jarvisWidget.addClass("jarviswidget").addClass("jarviswidget-color-darken");

        var jarvisHeader = $("<header role='heading'></header>");

        var title = $("<h2></h2>");

        var textTitle = $("<span class='fa fa-info'></span>");

        textTitle.append(defaults.title);

        title.append(textTitle);

        jarvisHeader.append(title);

        jarvisWidget.append(jarvisHeader);

        var jarvisBody = $("<div></div>");

        jarvisBody.addClass("widget-body").addClass("no-padding");

        jarvisBody.append(CreateTableHelper());

        auxiliarDiv.append(jarvisBody);

        jarvisWidget.append(auxiliarDiv);

        return jarvisWidget;
    }

    function CreateTableHelper() {

        var table = $("<table></table>");

        table.attr("id", ("datatable-" + firstPart + defaults.element));

        table.append($("<thead></thead>"));

        table.append($("<tbody></tbody>"));

        table.addClass("table").addClass("table-responsive").addClass("table-bordered");

        table.css("width", "100%");

        return table;

    }

    function GetColumnXEntity() {

        var columns = [];

        var currentEntity = defaults.entityName;

        $.ajax({
            url: "/Filter/GetJsonListColumnsHerlpersValueForEntity",

            data: { entity: currentEntity },

            type: "GET",

            async: false,

            success: function (columnsData) {

                columns = columnsData;

            },
            error: function () {

            },
            complete: function () {

            }
        });

        return columns;

    }

})(jQuery);
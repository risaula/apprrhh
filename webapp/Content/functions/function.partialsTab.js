﻿
(function ($) {

    var nextTabNumber = 2;

    var currentDataSource = [];

    var currentValue;

    var settings = {

        KeyPartialView: "",

        KeyControl: "",

        CurrentEntity: "",

        UrlActionGetPartial: "",

        UrlActionSavePartial: "",

        UrlActionUpdateControl: ""

    };

    $.fn.partialCreator = function (userSettings) {

        currentValue = this.value;


        settings = $.extend(settings, userSettings);

        $(this).attr("data-classEntity", settings.CurrentEntity);

        $(this).attr("data-added", true);

        initialize(this);

        bindingEvents(this);


    }

    function bindingEvents(control) {

        var idControl = $(control).attr("id");

        $('#btnCreatePartial-' + idControl + "-" + settings.CurrentEntity + "-" + settings.KeyPartialView + "-" + (nextTabNumber + 1))
            .on("click",
            function () {

                var currentEntity = $(this).attr("data-CurrentEntity");

                var urlAction = $(this).attr("data-UrlActionGetPartial");

                var actionUpdateControl = $(this).attr("data-actionUpdateControl");

                var fieldKey = $(this).attr("data-fieldKey");

                if (verificateExistPartialEntity(currentEntity)) {

                } else {

                    if (urlAction !== "") {
                        //var currentTab = $(".nav-tabs .active").attr('id');
                        //var tabControl = $.cookie('tabControl');
                        //if (tabControl) {
                        //    $.cookie('tabControl', `${tabControl},${currentTab}`);
                        //}
                        //else
                        //    $.cookie('tabControl', currentTab);
                        //alert($.cookie('tabControl'));
                        getPartialView(urlAction, currentEntity, actionUpdateControl, idControl + "_" + (nextTabNumber), fieldKey);

                    }

                }

            });




    }

    function getPartialView(urlAction, currentEntity, actionUpdateControl, controlToUpdate, fieldKey) {

        $.ajax({

            url: urlAction,

            type: "GET",

            success: function (partialView) {

                var nextTab = getNextTab();

                addTab(currentEntity, nextTab);

                addPartialView(nextTab, partialView, currentEntity, urlAction, actionUpdateControl, controlToUpdate, fieldKey);

            }

        });

    }

    function bindingEventsForms(nextTab, controlToUpdate) {

        var button = '#btn-save-form-tabs-' + nextTab;


        $("#buttonCloseTabs-" + nextTab)
            .on("click",
            function () {

                var currentEntity = $(this).attr("data-entity");

                var numberCurrentItem = $(this).attr("data-itemNumber");

                $("#s-next-" + numberCurrentItem).remove();

                var returnTab = $("li[data-currentEntity=" + currentEntity + "]").attr('caller').split('_')[0];

                $("li[data-currentEntity=" + currentEntity + "]").remove();

                deleteTab(currentEntity);

                //if (tabControl.length === 1) {
                //    $.removeCookie("tabControl");
                //    $(`#${tabControl[0]} a`).tab('show');
                //    return;
                //}

                //alert(`tabControl: ${tabControl.join(',')}`);

                //$.cookie("tabControl", tabControl.join(','));

                //if (pop) {
                //    $(`#${returnTab2} a`).tab('show');
                //    tabControl.pop();
                //    return;
                //}

                $(`#${returnTab} a`).tab('show');

            });

        $(button).on("click",
            function () {

                var form = "#" + $(this).attr("data-form");

                var urlAction = $(form).attr("data-action");

                var urlActionUpdateControl = $(form).attr("data-actionUpdateControl");

                var currentEntity = $(form).attr("data-entity");

                var numberCurrentItem = $(form).attr("data-numberItem");

                var fieldKey = $(form).attr("data-fieldKey");

                $.ajax({

                    url: urlAction,

                    type: "POST",

                    data: $(form).serialize(),

                    success: function (jsonResult) {

                        if (jsonResult.Success) {

                            globalFunctions.ShowNotificationSuccess(jsonResult.Message, jsonResult.Title);

                            CreatePullRequestPartialTab({
                                UrlActionUpdateControl: urlActionUpdateControl,
                                Entity: currentEntity,
                                IdEntity: jsonResult.IdEntity,
                                IdElementToUpdate: controlToUpdate,
                                FieldKey: fieldKey
                            });

                            $("#s-next-" + numberCurrentItem).remove();

                            var returnTab = $("li[data-currentEntity=" + currentEntity + "]").attr('caller').split('_')[0];

                            $("li[data-currentEntity=" + currentEntity + "]").remove();

                            //$('#s-init-partial-li').addClass('active');

                            //$('.nav-tabs li:eq(0) a').tab('show');
                            //var tabControl = $.cookie('tabControl').split(',');
                            //var returnTab = tabControl[tabControl.length - 1];
                            //tabControl.pop();
                            //$.cookie("tabControl", tabControl.join(','));

                            deleteTab(currentEntity);

                            $(`#${returnTab} a`).tab('show');

                        } else {
                            globalFunctions.ShowNotificationError(jsonResult.Message, jsonResult.Title);
                        }

                    }

                });

            });

    }

    function deleteTab(currentEntity) {
        //var tabControl = $.cookie('tabControl').split(',');
        //var returnTab2 = tabControl[tabControl.length - 1];
        //var pop = true;
        for (var iterator = 0; iterator <= nextTabNumber; iterator++) {
            var toDelete = $("li[caller=" + currentEntity + "_" + iterator + "]");
            if (toDelete !== undefined && toDelete) {
                //pop = false;
                //alert(`find: ${currentEntity}_${iterator}; remove: ${toDelete.attr("tabid")}`);
                $("#s-next-" + toDelete.attr('tabid')).remove();
                toDelete.remove();

                var entity = toDelete.attr("data-currentEntity");
                if (entity !== undefined) {
                    deleteTab(entity);
                }
            }
        }

    }

    function addPartialView(nextTab, partialView, currentEntity, urlAction, actionUpdateControl, controlToUpdate, fieldKey) {

        var formPartial = $("<form data-numberItem='" + nextTab + "' data-actionUpdateControl='" + actionUpdateControl + "' data-action='" + urlAction + "' id='form-tabs-" + nextTab + "' data-entity='" + currentEntity + "' data-fieldKey='" + fieldKey + "'></form>");

        formPartial.append(partialView);

        var nextDivContent = $('<div class="tab-pane fade in" id="s-next-' + nextTab + '"></div>');

        var jarvisWidget = $('<div class="jarviswidget jarviswidget-color-darken"></div>');

        var jarvisHeader = $('<header role="heading"></header>').append('<h2><span class="fa fa-plus-square"></span>  ' + currentEntity + ' </h2>');

        jarvisWidget.append(jarvisHeader);

        var jarvisBody = $('<div></div>').append($('<div class="widget-body"></div>').append(formPartial).append('<div class="widget-footer"><button type="button" id="btn-save-form-tabs-' + nextTab + '" class="btn btn-primary" data-form="form-tabs-' + nextTab + '"><i class="fa fa-save"></i> Guardar</button></div>'));

        jarvisWidget.append(jarvisBody);

        nextDivContent.append(jarvisWidget);

        $("#content-partialTabs").append(nextDivContent);

        $('.nav-tabs li:eq(' + (nextTab - 1) + ') a').tab('show');

        bindingEventsForms(nextTab, controlToUpdate);

    }

    function addTab(currentEntity, nextTab) {
        //alert(nextTabNumber);
        var caller = $(".nav-tabs .active").attr('id');
        var tab = '<li data-currentEntity="' + currentEntity + '" tabid="' + nextTab + '" id="' + currentEntity + '" caller="' + caller + "_" + (nextTab - 1) + '">' + "<span class='air air-top-left delete-tab' style='top: 7px; left: 7px;'><button class='btn btn-xs font-xs btn-default hover-transparent' id='buttonCloseTabs-" + nextTab + "' data-entity='" + currentEntity + "' data-itemNumber='" + nextTab + "'><i class='fa fa-times'></i></button></span>" + '<a href="#s-next-' + nextTab + '" data-toggle="tab"><i></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ' + currentEntity + '</a></li>';

        $("#ulPartialsTabs").append(tab);

    }

    function getNextTab() {
        var len = getLength();
        if (len === 1)
            nextTabNumber = 2;
        else if (len < nextTabNumber)
            nextTabNumber = len + 1;
        return nextTabNumber++;
    }

    function getLength() {

        return $('#tabs-partialTabs ul li').length;

    }

    function initialize(control) {

        drawHelperControl(control);

    }

    function setValuesControl(control, data, currentValue) {

        $(control).empty();

        $(control).select2({ data: currentDataSource });

        $(control).val(currentValue).trigger("change");

    }


    function verificateExistPartialEntity(newEntity) {

        var controls = $('#tabs-partialTabs ul li');

        var listaEntidades = new Array();

        $.each(controls,
            function (idx, ctr) {

                var entidad = $(ctr).attr("data-currentEntity");

                console.log(entidad);

                listaEntidades.push(entidad);

            });

        var valueFound = false;

        $.each(listaEntidades,
            function (idx, value) {

                if (value === newEntity) {

                    valueFound = true;

                }

            });

        return valueFound;

    }


    function drawHelperControl(control) {

        var idControl = $(control).attr("id");

        //alert($(control).attr("data-fieldKey"));

        $(control).attr("idForUpdate", idControl + "_" + (nextTabNumber + 1));

        var id = 'btnCreatePartial-' + idControl + "-" + settings.CurrentEntity + '-' + settings.KeyPartialView + '-' + (nextTabNumber + 1);

        var button = $('<button class="pull-right btn btn-link" type="button" id="' + id + '"> <i class="fa fa-plus"> </i>  </button>');


        button.attr("data-CurrentEntity", settings.CurrentEntity);

        button.attr("data-KeyPartialView", settings.KeyPartialView);

        button.attr("data-UrlActionGetPartial", settings.UrlActionGetPartial);

        button.attr("data-actionUpdateControl", settings.UrlActionUpdateControl);

        button.attr("data-id-update", idControl);

        button.attr("data-fieldKey", $(control).attr("data-fieldKey"));

        button.css("padding-right", "10px");

        $(control).closest("div").before(button);

    }

})(jQuery);
﻿

var globalOptionsExecuteAction = {
    Action: null,
    Controller: null,
    ShowNotification: true,
    FlagIndicatorSuccessNotification: "Success",
    ReloadTable: false,
    ElementReloadTable: null,
    UseUrlAction: false,
    UrlAction: null,
    params: null,
    type: "GET"
};

var globalFunctions = {
    //var obj = { Message: "", Success : true or false, Title:"" };

    Initialize: function () {

        globalFunctions.ValidateInputsTypeNumber();

    },

    LoadWaitNotification: function () {
        $("#modalWaitingTime").modal({ backdrop: 'static', keyboard: false }, "show");
        $('#statusID').fadeIn();
    },

    UnloadWaitNotification: function () {
        $("#modalWaitingTime").modal("hide");
        $('#statusID').hide();
    },

    ShowNotificationSuccess: function (message, title) {
        $.smallBox({
            title: title,
            content: message,
            color: "#739E73",
            timeout: 8000,
            icon: "fa fa-check shake animated"
        });
    },

    ShowNotificationError: function (message, title) {
        $.smallBox({
            title: title,
            content: message,
            color: "#C46A69",
            timeout: 8000,
            icon: "fa fa-warning shake animated"
        });
    },

    ExecuteAction: function (optionsExecuteAction) {

        globalOptionsExecuteAction.type = "GET";
        

        var options = $.extend(globalOptionsExecuteAction, optionsExecuteAction);

        var urlAction = ("/_controller_/_action_").replace("_controller_", options.Controller).replace("_action_", options.Action);

        var ajaxsetting = {
            url: urlAction,
            type: options.type,
            success: function (response) {
                if (options.ShowNotification) {
                    if (response[options.FlagIndicatorSuccessNotification]) {
                        globalFunctions.ShowNotificationSuccess(response.Message, response.Title);
                    } else {
                        globalFunctions.ShowNotificationError(response.Message, response.Title);
                    }
                }
                if (options.ReloadTable) {
                    if (options.UseUrlAction) {
                        $("#" + options.ElementReloadTable).MiddlewareReloadDataTableByUrl(options.UrlAction);
                    } else {
                        $("#" + options.ElementReloadTable).MiddlewareReloadDataTable();
                    }
                }
            },
            error: function () {
            },
            complete: function () {
            }
        };

        if (options.params != null) ajaxsetting["data"] = options.params;


        $.ajax(ajaxsetting);

    },

    ChangeCurrentCurrency: function (button) {
        $.ajax({
            url: '/Base/SetCurrentCurrency',
            type: "POST",
            data: { idCurrency: $(button).attr('data-id') },
            success: function (data) {

                $("#CurrencyMenu").remove();

                $.ajax({
                    url: '/Base/GetCurrencies',
                    type: "GET",
                    data: null,
                    success: function (data) {
                        $("#CurrencyLayout").append(data);
                        globalFunctions.setCurrentCurrencyInView();
                    },
                    error: function (data) {
                        //alert("error");
                    }
                });

            }
        });
    },


    ChangeCurrentLanguage: function (button) {
        $.ajax({
            url: '/Base/SetCurrentLanguage',
            type: "POST",
            data: { idLanguage: $(button).attr('data-id') },
            success: function (data) {
                $.each($("*[data-class=Label-ML],*[data-class=PlaceHolderMultiLanguage]"), function () {
                    var idDictionary = $(this).attr("data-IdDicitionary");
                    var result = data.Messages.filter(function (obj) { if ('IdDictionary' in obj && parseInt(idDictionary) === parseInt(obj.IdDictionary)) { return true; } return false; });
                    $(this).text(result.length > 0 ? result[0].Message : "");
                    $(this).attr("placeholder", result.length > 0 ? result[0].Message : "");
                });
                $("#LanguagesMenu").remove();
                $.ajax({
                    url: '/Base/GetLanguages',
                    type: "GET",
                    data: null,
                    success: function (data) {
                        $("#LanguageLayout").append(data);
                    },
                    error: function (data) {
                        alert("error");
                    }
                });
            }
        });
    },

    ValidateInputsTypeNumber: function () {
        $("input[type=number]").keydown(function (e) {
            var key = e.charCode || e.keyCode || 0;
            // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
            // home, end, period, and numpad decimal
            return (
                key == 8 ||
                key == 9 ||
                key == 13 ||
                key == 46 ||
                key == 110 ||
                key == 190 ||
                (key >= 35 && key <= 40) ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
        });
    },

    initializePartialTabs: function () {

    },

    getAttachedFiles: function (id, entity) {

        $.ajax({

            url: "/Base/GetAttachedFiles",

            data: {

                idEntity: id,

                entity: entity

            },

            type: "GET",

            success: function (partialView) {

                $("#ModalGetAttachedFilesBody").html(partialView);

                $("#ModalGetAttachedFiles").modal().show();

            }

        });

    },

    applyPartialCreator: function () {


        $.each($('table[data-class="PartialTabControl"]'),
            function (idx, dt) {

                var control = dt;

                var urlUpdateControl = $(control).attr("data-urlactionupdatecontrol");

                var urlGetPartialView = $(control).attr("data-urlactiongetpartialtab");

                var entity = $(control).attr("data-entity");

                var added = $(control).attr("data-added");

                if (added === "true") {

                } else {

                    $(control).partialCreator({
                        CurrentEntity: entity,
                        UrlActionGetPartial: urlGetPartialView,
                        UrlActionUpdateControl: urlUpdateControl
                    });

                }

            });


        $.each($('select[data-class="PartialTabControl"]'),
            function (idx, dt) {

                var control = dt;

                var urlUpdateControl = $(control).attr("data-urlactionupdatecontrol");

                var urlGetPartialView = $(control).attr("data-urlactiongetpartialtab");

                var entity = $(control).attr("data-entity");

                var added = $(control).attr("data-added");

                if (added==="true") {
                    
                } else {

                    $(control).partialCreator({
                        CurrentEntity: entity,
                        UrlActionGetPartial: urlGetPartialView,
                        UrlActionUpdateControl: urlUpdateControl
                    });

                }

            });

    },

    setCurrentCurrencyInView: function () {

        var currentCurrency = $("a[data-currentCurrency=true]");

        var currency = $(currentCurrency).attr("data-key");

        console.log(currency);

        var controls = $("*[data-classentity=Currency]");

        var controlsTables = $("table[data-dataType=Currency]");

        console.log(controls);

        $.each(controls,
            function (idx, dt) {

                $(dt).val(currency).trigger("change");

            });

        $.each(controlsTables,
            function (idx, dt) {

                var idTable = $(dt).attr("id");

                var table = new $.fn.dataTable.Api("#" + idTable);

                table.ajax.reload();

            });


    }

};




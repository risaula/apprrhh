﻿function GetAutoComplete() {

    var inputsWithAutoComplete = $("input[data-class=autocomplete]");


    $.each(inputsWithAutoComplete,
        function (idx, dt) {

            var entity = $(this).attr("data-entity");

            var selector = $(this).attr("id");



            $("#" + selector).on("keypress",
                function () {

                    $("#" + selector).css("color", "black");

                    $("#" + selector).css("border-color", "#BDBDBD");

                });

            $.ajax({

                url: "/Base/GetAutoCompleteForEntity",

                data: {

                    entity: entity

                },

                success: function (data) {

                    $("#" + selector).autocomplete({

                        source: data.AutoComplete,

                        select: function (e) {
                            
                            var dataChangeColor = $(e.target).attr("data-validate");

                            if (dataChangeColor === "True") {

                                $(e.target).css("color", "red");

                                $(e.target).css("border-color", "red");

                            }

                        }

                    });

                }

            });

        });

}
﻿
(function ($) {


    var optionsPluginsForms = {
        resetForm: true,
        updateTable: false,
        usingUrl: false,
        urlUpdate: null,
        elementTable: null,
        closeModal: false,
        elementModal: null,
        showBoxNotification: true,
        indicatorFlag: "Success"
    };

    $.fn.MiddlewareSaveForms = function (action, controller, options) {
        var form = this;
        var idForm = "#" + form.attr("id");
        if (!$(form).is("form")) {
            ShowboxError("It's not a form", "Save Forms");
            return false;
        }

        if (action == "" || controller == "") {
            ShowboxError("It's not a form", "Save Forms");
            return false;
        }

        $validator = $(idForm).validate();
        var $valid = $(idForm).valid();
        optionsPluginsForms = $.extend(optionsPluginsForms, options);
        if (!$valid) {
            $validator.focusInvalid();
            return false;
        } else {
            var urlAction = ("/_controller_/_action_").replace("_controller_", controller).replace("_action_", action);
            $.ajax({
                url: urlAction,
                type: "POST",
                data: $(form).serialize(),
                success: function (response) {
                    if (optionsPluginsForms.showBoxNotification) {
                        if (response[optionsPluginsForms.indicatorFlag]) {
                            ShowboxSuccess(response.Message, response.Title);
                        } else {
                            ShowboxError(response.Message, response.Title);
                            return false;
                        }
                    }
                    if (optionsPluginsForms.closeModal) {
                        $("#" + optionsPluginsForms.elementModal).modal("hide");
                    }
                    if (optionsPluginsForms.updateTable) {
                        if (optionsPluginsForms.usingUrl) {
                            $("#" + optionsPluginsForms.elementTable).MiddlewareReloadDataTableByUrl(optionsPluginsForms.urlUpdate);
                        } else {
                            $("#" + optionsPluginsForms.elementTable).MiddlewareReloadDataTable();
                        }
                    }
                    if (optionsPluginsForms.resetForm) {
                        ClearControls(form);
                    }
                    return true;
                },
                error: function () {
                    return false;
                },
                complete: function () {
                    return true;
                }
            });
        }
    }

    function ClearControls(form) {
        var inputs = $(form).find("input");
        $.each(inputs, function (idx, dt) {
            $(dt).val("").change();
        });
        var textAreas = $(form).find("textarea");
        $.each(textAreas, function (idx, dt) {
            $(dt).val("").change();
        });

    }

})(jQuery);
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartAdminMvc.Models.Observaciones
{
    public class EditarObservacionesViewModel :CrearObservacionesViewModel
    {
        public int IdObservacion { get; set; }
        public bool Estado { get; set; }
    }
}
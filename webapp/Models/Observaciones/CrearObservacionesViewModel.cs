﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartAdminMvc.Models.Observaciones
{
    public class CrearObservacionesViewModel
    {
        [Required]
        [Display(Name = "Persona")]
        public int IdPersona { get; set; }
     
        [Required]
        [Display(Name = "Descripción")]
        public string Descripcion { get; set; }
    }
}
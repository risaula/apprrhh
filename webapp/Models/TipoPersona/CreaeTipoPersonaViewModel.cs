﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartAdminMvc.Models.TipoPersona
{
    public class CreaeTipoPersonaViewModel
    {
        public string Nombre { get; set; }
    }
}
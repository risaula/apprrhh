﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartAdminMvc.Models.Puesto
{
    public class CrearPuestioViewModel
    {
        [Required]
        [Display(Name = "Puesto")]
        public string Puesto { get; set; }
        [Required]
        [Display(Name = "Area")]
        public int IdArea { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartAdminMvc.Models.Puesto
{
    public class EditarPuestoViewModel : CrearPuestioViewModel
    {
        public int IdPuesto { get; set; }
        public bool Estado { get; set; }
    }
}
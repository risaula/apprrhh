﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartAdminMvc.Models.Genero
{
    public class CrearGeneroViewModel
    {
        [Required]
        [Display(Name = "Genero")]
        public string Genero { get; set; }
    }
}
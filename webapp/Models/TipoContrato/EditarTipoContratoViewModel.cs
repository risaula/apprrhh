﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartAdminMvc.Models.TipoContrato
{
    public class EditarTipoContratoViewModel : CraerTipoContratoViewModel
    {
        public int IdContrato { get; set; }
        public bool Estado { get; set; }
    }
}
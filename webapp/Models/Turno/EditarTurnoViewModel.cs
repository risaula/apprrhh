﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartAdminMvc.Models.Turno
{
    public class EditarTurnoViewModel : CrearTurnoViewModel
    {
        public int IdTurno { get; set; }
        public bool Estado { get; set; }
    }
}
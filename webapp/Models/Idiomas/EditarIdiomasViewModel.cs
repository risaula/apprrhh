﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartAdminMvc.Models.Idiomas
{
    public class EditarIdiomasViewModel : CrearIdiomasViewModel
    {
        public int IdIdioma { get; set; }
        public bool EstadoIdioma { get; set; }
    }
}
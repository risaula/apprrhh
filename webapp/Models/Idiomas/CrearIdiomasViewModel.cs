﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartAdminMvc.Models.Idiomas
{
    public class CrearIdiomasViewModel
    {
        [Required]
        [Display(Name = "Idioma")]
        public string NombreIdioma { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartAdminMvc.Models.EstadoCivil
{
    public class EditarEstadoCivilViewModel : CrearEstadoCivilViewModel
    {
        public int IdEstadoC { get; set; }
        public bool EstadoEstadoCivil { get; set; }
    }
}
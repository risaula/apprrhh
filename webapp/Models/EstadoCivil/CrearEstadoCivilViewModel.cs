﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartAdminMvc.Models.EstadoCivil
{
    public class CrearEstadoCivilViewModel
    {
        [Required]
        [Display(Name = "Estado Civil")]
        public string EstadoCivil { get; set; }
    }
}
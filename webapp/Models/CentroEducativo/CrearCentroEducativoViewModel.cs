﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartAdminMvc.Models.CentroEducativo
{
    public class CrearCentroEducativoViewModel
    {


        [Required(ErrorMessage = "Nombre Requerido")]
        public string Nombre { get; set; }
        [Required(ErrorMessage = "Telefono Requerido")]
        public string Telefono { get; set; }
        [Required(ErrorMessage = "Direccion Requerida")]
        public string Direccion { get; set; }

    }
}
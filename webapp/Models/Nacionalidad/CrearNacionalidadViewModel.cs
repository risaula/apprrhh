﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartAdminMvc.Models.Nacionalidad
{
    public class CrearNacionalidadViewModel
    {
        [Required]
        [Display(Name = "Nacionalidad")]
        public string Nacionalidad { get; set; }
        [Required]
        [Display(Name = "Numero Identidad")]
        public string NumeroIdentidad { get; set; }
        [Required]
        [Display(Name = "Telefono")]
        public string Telefono { get; set; }
    }
}
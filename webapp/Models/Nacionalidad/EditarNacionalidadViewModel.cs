﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartAdminMvc.Models.Nacionalidad
{
    public class EditarNacionalidadViewModel: CrearNacionalidadViewModel
    {
        public int IdNac { get; set; }

        public bool Estado { get; set; }
    }
}
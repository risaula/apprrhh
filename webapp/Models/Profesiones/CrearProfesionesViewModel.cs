﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartAdminMvc.Models.Profesiones
{
    public class CrearProfesionesViewModel
    {
        [Required]
        [DisplayName("Nivel Educativo")]
        public int IdNivelEducativo { get; set; }

        [Required]
        [DisplayName("Profesiones")]
        public string Descripcion { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace SmartAdminMvc.Models.Profesiones
{
    public class EditarProfesionesViewModel : CrearProfesionesViewModel
    {
        public int IdProfesione { get; set; }
        public bool Estado { get; set; }
    }
}
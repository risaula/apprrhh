﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartAdminMvc.Models.Ingresos
{
    public class EditarIngresosViewModel : CrearIngresoViewModel
    {
        public int IdIngresos { get; set; }

        public bool EstadoIngresos { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartAdminMvc.Models.Personas
{
    public class CrearCandidatoViewModel

    {

    public string Persona { get; set; }
    public bool TipoPerso { get; set; }
    public int IdPersona { get; set; }
        [Required]
        [Display(Name = "Telefono Fijo")]
        public string TelefonoFijos { get; set; }
        [Required(ErrorMessage = "Nombre Requerido")]
        [Display(Name = "Nombre")]
        public string Nombres { get; set; }
        [Required]
        [Display(Name = "Apellido")]
        public string Apellido { get; set; }
        [Required]
        [Display(Name = "Numero de Identidad")]
        public string NumeroIdentidads { get; set; }
        [Required]
        [Display(Name = "Direccion")]
        public string Direccions { get; set; }
        [Required]
        [Display(Name = "Telefono Movil")]
        public string TelefonoMovils { get; set; }

    [DataType(DataType.DateTime)]
    [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MMMM/yyyy}")]
    public DateTime FechaNacimientos { get; set; }

     [DataType(DataType.DateTime)]
    [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MMMM/yyyy}")]
    public DateTime FechaInic { get; set; }

        
    public bool Estado { get; set; }
    public bool PoseeVehiculos { get; set; }
        [Required]
        [Display(Name = "Genero")]
        public string Generos { get; set; }
        [Required]
        [Display(Name = "Estado Civil")]
        public string EstadoCivils { get; set; }
        [Required]
        [Display(Name = "Nacionalidad")]
        public string Nacionalidads { get; set; }
    public string Puestos { get; set; }
    public int idGeneros { get; set; }
    public int idTurno { get; set; }
    public int idEstadoCivils { get; set; }
    public int idNacionalidads { get; set; }
    public int idPuestos { get; set; }
    public string Contrato { get; set; }
    public int idTipoPlan { get; set; }
    public string TipoPlan { get; set; }
    public int idTipoContra { get; set; }
    public string TipoContra { get; set; }
    public int idTipoTurno { get; set; }
    public string TipoTurno { get; set; }
   public string HistorialSueldo { get; set; }
        public string TPuesto { get; set; }
        [Required]
        [Display(Name = "Sueldo Base")]
        public decimal SueldoBase { get; set; }
 
        public bool EstadoContrato { get; set; }


        [Required]
        [Display(Name = "Codigo Empleado")]
        public int CodigoEmpleado { get; set; }



        //public List<NivelEducativoPorPersonaViewModel> NivelesEducativosPorPersona { get; set; }

        //public List<FamiliaresPorPersonaViewModel> FamiliaresPorPersona { get; set; }

        //public List<AntecedentesPorPersonaViewModel> AntecedentesPorPersona { get; set; }

        //public List<ParientesPorPersonaViewModel> ParientesPorPersona { get; set; }

        //public List<ReferenciasLaboralesPorPersonaViewModel> ReferenciasLaboralesPorPersona { get; set; }
    }
}
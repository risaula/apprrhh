﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartAdminMvc.Models.Personas
{
    public class GetInfoPersonas :CrearCandidatoViewModel
    {
        [Required]
        public int Fk_IdPuesto { get; set; }
        [Required]
        public int Fk_IdTipoContrato { get; set; }
        [Required]
        public int Fk_IdTipoPlanilla { get; set; }
        [Required]
        public int Fk_IdTurno { get; set; }
        [Required]
        public int Fk_IdCompania { get; set; }
        [Required]
        public decimal SueldoBase { get; set; }
        [Required]
        public bool EstadoContrato { get; set; }
        [Required]
        public DateTime FechaInicio { get; set; }
        [Required]
        public int CodigoEmpleado { get; set; }
    }
}
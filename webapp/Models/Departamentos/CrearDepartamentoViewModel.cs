﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartAdminMvc.Models.Departamentos
{
    public class CrearDepartamentoViewModel
    {
        [Required]
        [Display(Name = "Departamento")]
        public string NombreDepartamento { get; set; }



    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartAdminMvc.Models.Departamentos
{
    public class EditarDepartamentoViewModel : CrearDepartamentoViewModel
    {
        public int IdDepartamento { get; set; }
        public string EstadoDepartamento { get; set; }
    }
}
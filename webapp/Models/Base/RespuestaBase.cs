﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartAdminMvc.Models.Base
{
    public class RespuestaBase
    {
        public bool Estatus { get; set; }
        public string Mensage { get; set; }
        public string Titulo { get; set; }
    }
}
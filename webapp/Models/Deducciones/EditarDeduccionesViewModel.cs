﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartAdminMvc.Models.Deducciones
{
    public class EditarDeduccionesViewModel : CrearDeduccionesViewModel
    {
        public int IdDeducciones { get; set; }
        public bool EstadoDeducciones { get; set; }

    }
}
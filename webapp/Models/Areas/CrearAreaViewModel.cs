﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartAdminMvc.Models.Areas
{
    public class CrearAreaViewModel
    {
        [Required]
        [Display(Name = "Departamento")]
        public int IdDepartamento { get; set; }

        [Required]
        [Display(Name = "Descripción")]
        public string NombreArea { get; set; }
    }
}
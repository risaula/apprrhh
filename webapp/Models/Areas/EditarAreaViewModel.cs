﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartAdminMvc.Models.Areas
{
    public class EditarAreaViewModel : CrearAreaViewModel
    {
        public int IdArea { get; set; }
        public bool Estado { get; set; }

    }
}
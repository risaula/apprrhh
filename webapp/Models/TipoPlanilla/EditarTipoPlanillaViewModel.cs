﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartAdminMvc.Models.TipoPlanilla
{
    public class EditarTipoPlanillaViewModel : CrearTipoPlanillaViewModel
    {
        public int idTipoPlanilla { get; set; }

        public bool Estado { get; set; }
    }
}